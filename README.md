## Website
[www.vaxtracker.me](https://vaxtracker.me)

## Team Info
| Name | EID | GitLabID |
| ------ | ------ | ------ |
| Anjitha Nair | akn698 | @anjithaknair |
| Lilia Li | st33578 | @liliali2000 |
| Evan Seils | ezs227 | @archevan |
| Mingkang Zhu | mz8374 | @mkzhu2000 |
| Seth Erfurt | ske397 | @Subject38 |

## Phase One
Project Leader: Lilia Li
| Name | Estimated | Actual |
| ------ | ------ | ------ |
| Anjitha Nair | 14 hrs | 21 hrs |
| Lilia Li | 12 hrs | 25 hrs |
| Evan Seils | 12 hrs | 17 hrs |
| Mingkang Zhu | 15 hrs | 21 hrs |
| Seth Erfurt | 13 hrs | 20 hrs |

## Phase Two
Project Leader: Anjitha Nair
| Name | Estimated | Actual |
| ------ | ------ | ------ |
| Anjitha Nair | 35 hrs | 50 hrs |
| Lilia Li | 40 hrs | 55 hrs |
| Evan Seils | 35 hrs | 50 hrs |
| Mingkang Zhu | 35 hrs | 50 hrs |
| Seth Erfurt | 35 hrs | 55 hrs |

## Phase Three
Project Leader: Seth Erfurt
| Name | Estimated | Actual |
| ------ | ------ | ------ |
| Anjitha Nair | 20 hrs | 20 hrs |
| Lilia Li | 40 hrs | 40 hrs |
| Evan Seils | 20 hrs | 20 hrs |
| Mingkang Zhu | 35 hrs | 35 hrs |
| Seth Erfurt | 15 hrs | 25 hrs |

## Phase Four
Project Leader: Mingkang Zhu
| Name | Estimated | Actual |
| ------ | ------ | ------ |
| Anjitha Nair | 10 hrs | 10 hrs |
| Lilia Li | 15 hrs | 15 hrs |
| Evan Seils | 10 hrs | 10 hrs |
| Mingkang Zhu | 10 hrs | 10 hrs |
| Seth Erfurt | 10 hrs | 10 hrs |

## Git SHA
863085f0ffe777b350de99b545fb9038f072eb5f

## Video Presentation
https://youtu.be/mLxzFAj7O3w

## Postman API Documentation
[https://documenter.getpostman.com/view/14740899/TzRNEUuD](https://documenter.getpostman.com/view/14740899/TzRNEUuD)

## GitLab Pipelines
[https://gitlab.com/cs373-team2/vax-tracker/-/pipelines](https://gitlab.com/cs373-team2/vax-tracker/-/pipelines)

## Docker Instructions
The following two commands will spin up containers for the frontend and backend:
```
docker-compose up --build --detach
```
