import Home from './pages/home/Home'
import About from './pages/about/About'
import ClinicalTrials from './pages/clinical-trials/ClinicalTrials'
import GlobalVacc from './pages/countries/GlobalVacc'
import Beliefs from './pages/beliefs/Beliefs'
import Vaccines from './pages/vaccines/Vaccines'
import CountryInstance from './pages/countries/CountryInstance'
import VaccinesInstance from './pages/vaccines/VaccinesInstance'
import ClinicalTrialsInstance from './pages/clinical-trials/ClinicalTrialsInstance'
import BeliefsInstance from './pages/beliefs/BeliefsInstance'
import OurVisualizations from './pages/our-visualizations/OurVisualizations'
import ProviderVisualizations from './pages/provider-visualizations/ProviderVisualizations'

import Search from './pages/search/Search'

const Routes = [
    {
        title: 'Home',
        path: '/',
        exact: true,
        Component: Home,
    },
    {
        title: 'About',
        path: '/about',
        exact: true,
        Component: About,
    },
    {
        title: 'Search',
        path: '/search',
        exact: true,
        Component: Search,
    },
    {
        title: 'Clinical Trials',
        path: '/clinicaltrials',
        exact: true,
        Component: ClinicalTrials,
    },
    {
        path: '/clinicaltrials/:id',
        Component: ClinicalTrialsInstance,
    },
    {
        title: 'Global Vaccinations',
        path: '/globalvacc',
        exact: true,
        Component: GlobalVacc,
    },
    {
        path: '/globalvacc/:id',
        Component: CountryInstance,
    },
    {
        title: 'Beliefs',
        path: '/beliefs',
        exact: true,
        Component: Beliefs,
    },
    {
        path: '/beliefs/:id',
        Component: BeliefsInstance,
    },
    {
        title: 'Vaccines',
        path: '/vaccines',
        exact: true,
        Component: Vaccines,
    },
    {
        path: '/vaccines/:id',
        Component: VaccinesInstance,
    },
    {
        title: 'Our Visualizations',
        path: '/OurVisualizations',
        exact: true,
        Component: OurVisualizations,
    },
    {
        title: 'Provider Visualizations',
        path: '/ProviderVisualizations',
        exact: true,
        Component: ProviderVisualizations,
    },
]

export default Routes
