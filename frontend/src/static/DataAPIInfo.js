import World from '../pages/about/images/World.png'
import Clinic from '../pages/about/images/Clinic.png'
import Vaccine from '../pages/about/images/Vaccine.png'
import Covid from '../pages/about/images/Covid.png'
import FaceMask from '../pages/about/images/FaceMask.jpg'

const data_api_info = [
    {
        title: 'Data for Covid Related Belief 1',
        image: Covid,
        link:
            'https://gisumd.github.io/COVID-19-API-Documentation/docs/home.html',
    },
    {
        title: 'Data for Covid Related Belief 2',
        image: FaceMask,
        link: 'https://disease.sh/docs/',
    },
    {
        title: 'World Vaccine Allocation',
        image: World,
        link: 'https://covidsurvey.mit.edu/api.html',
    },
    {
        title: 'Clinical Trials',
        image: Clinic,
        link: 'https://clinicaltrials.gov/api/gui',
    },
    {
        title: 'Data for Vaccine Information',
        image: Vaccine,
        link:
            'https://www.raps.org/news-and-articles/news-articles/2020/3/covid-19-vaccine-tracker',
    },
]

export { data_api_info }
