import anjitha from '../pages/about/images/anjitha.png'
import evan from '../pages/about/images/evan.jpeg'
import lilia from '../pages/about/images/lilia.jpeg'
import mingkang from '../pages/about/images/mingkang.jpeg'
import seth from '../pages/about/images/seth.png'

export const memberInfo = [
    {
        name: ['Anjitha Nair'],
        desc:
            "Anjitha is a junior CS major from Dallas, TX! She's interested in the intersection of technology and public policy. She loves listening to podcasts, keeping up with the news and current events, and trying out new recipes!",
        jobs: 'Phase 2 Leader | Fullstack Developer',
        image: anjitha,
        gitlab: 'anjithaknair',
        commits: 0,
        issues: 0,
        tests: 0,
        email: 'anjithaknair10@gmail.com',
    },
    {
        name: ['Evan Seils'],
        desc:
            'Evan is a junior CS major. He grew up in Belton, Texas — roughly an hour north of Austin. He likes rock climbing during free times.',
        jobs: 'Fullstack Developer',
        image: evan,
        gitlab: 'archevan',
        commits: 0,
        issues: 0,
        tests: 13,
        email: 'evanseils2013@gmail.com',
    },
    {
        name: ['Lilia Li'],
        desc:
            'Lilia is a junior CS student from The Woodlands, TX. Lilia enjoys cooking/baking, playing board games, and traveling in her free time!',
        jobs: 'Phase 1 Leader | Fullstack Developer',
        image: lilia,
        gitlab: 'liliali2000',
        commits: 0,
        issues: 0,
        tests: 12,
        email: 'liliali2000@utexas.edu',
    },
    {
        name: ['Mingkang Zhu'],
        desc:
            'Mingkang is a junior CS student at UT. He likes Machine Learning and Computer Vision.',
        jobs: 'Fullstack Developer',
        image: mingkang,
        gitlab: 'mkzhu2000',
        commits: 0,
        issues: 0,
        tests: 6,
        email: 'mkzhu2000@gmail.com',
    },
    {
        name: ['Seth Erfurt'],
        desc:
            "Seth is a junior CS student at UT. He's from Euless, TX. He has an interest in debugging and reverse engineering software. He also loves playing arcade music games in his spare time at Round 1.",
        jobs: 'Fullstack Developer | Phase 3 Leader',
        image: seth,
        gitlab: 'Subject38',
        commits: 0,
        issues: 0,
        tests: 32,
        email: 'seth@ske.moe',
    },
]
