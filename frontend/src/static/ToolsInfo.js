import BootstrapLogo from '../pages/about/images/BootstrapLogo.svg'
import AWSLogo from '../pages/about/images/AWSLogo.png'
import DockerLogo from '../pages/about/images/DockerLogo.png'
import GitLabLogo from '../pages/about/images/GitLabLogo.png'
import NamecheapLogo from '../pages/about/images/NamecheapLogo.png'
import SeleniumLogo from '../pages/about/images/SeleniumLogo.png'
import PostmanLogo from '../pages/about/images/PostmanLogo.png'
import ReactLogo from '../pages/about/images/ReactLogo.png'
import MySQLLogo from '../pages/about/images/MySQLLogo.png'
import MaterialUILogo from '../pages/about/images/MaterialUILogo.png'
import PandasLogo from '../pages/about/images/PandasLogo.png'
import FlaskLogo from '../pages/about/images/FlaskLogo.jpg'
import JestLogo from '../pages/about/images/JestLogo.png'
import SQLAlchemyLogo from '../pages/about/images/SQLAlchemyLogo.png'
import OpenAPILogo from '../pages/about/images/OpenAPILogo.png'

const toolsInfo = [
    {
        title: 'Docker',
        image: DockerLogo,
        description: 'Virtual development environment',
        link: 'https://docker.com/',
    },
    {
        title: 'GitLab',
        image: GitLabLogo,
        description: 'Git repository for version control',
        link: 'https://www.gitlab.com/',
    },
    {
        title: 'React',
        image: ReactLogo,
        description: 'Frontend',
        link: 'https://reactjs.org/',
    },
    {
        title: 'Bootstrap',
        image: BootstrapLogo,
        description: 'CSS Framework',
        link: 'https://getbootstrap.com/',
    },
    {
        title: 'AWS',
        image: AWSLogo,
        description: 'Website hosting platform',
        link: 'https://aws.amazon.com/',
    },
    {
        title: 'Postman',
        image: PostmanLogo,
        description: 'API design and testing',
        link: 'https://postman.com/',
    },
    {
        title: 'MySQL',
        image: MySQLLogo,
        description: 'Database storage',
        link: 'https://www.mysql.com/',
    },
    {
        title: 'Material UI',
        image: MaterialUILogo,
        description: 'React UI framework',
        link: 'https://material-ui.com/',
    },
    {
        title: 'Pandas',
        image: PandasLogo,
        description: 'Python library for data manipulation',
        link: 'https://pandas.pydata.org/',
    },
    {
        title: 'Flask',
        image: FlaskLogo,
        description: 'Web framework',
        link: 'https://flask.palletsprojects.com/en/1.1.x/',
    },
    {
        title: 'Selenium',
        image: SeleniumLogo,
        description: 'UI testing framework',
        link: 'https://www.selenium.dev/',
    },
    {
        title: 'Jest',
        image: JestLogo,
        description: 'Javascript frontend testing framework',
        link: 'https://jestjs.io/',
    },
    {
        title: 'Namecheap',
        image: NamecheapLogo,
        description: 'Domain name registrar',
        link: 'https://namecheap.com/',
    },
    {
        title: 'SQLAlchemy',
        image: SQLAlchemyLogo,
        description: 'SQL toolkit and object-relational mapper for Python',
        link: 'https://www.sqlalchemy.org/',
    },
    {
        title: 'OpenAPI 3.0 YAML',
        image: OpenAPILogo,
        description: 'A data-serialization language',
        link: 'https://swagger.io/specification/',
    },
]

export { toolsInfo }
