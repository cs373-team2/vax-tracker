import PostmanLogo from '../pages/about/images/PostmanLogo.png'
import GitLabLogo from '../pages/about/images/GitLabLogo.png'
import YoutubeLogo from '../pages/about/images/YouTubeLogo.png'

const projectInfo = [
    {
        title: 'GitLab Repository',
        image: GitLabLogo,
        link: 'https://gitlab.com/cs373-team2/vax-tracker',
    },
    {
        title: 'Postman API',
        image: PostmanLogo,
        link: 'https://documenter.getpostman.com/view/14740899/TzRNEUuD',
    },
    {
        title: 'Presentation',
        image: YoutubeLogo,
        link: 'https://youtu.be/mLxzFAj7O3w'
    }
]

export { projectInfo }
