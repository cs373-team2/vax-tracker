const models = [
    {
        title: 'Vaccines',
        desc:
            'Click to learn more about the various vaccines that are approved or in trial around the world.',
        imageUrl: process.env.PUBLIC_URL + 'vaccinesbanner.jpeg',
        href: '/Vaccines',
    },
    {
        title: 'Clinical Trials',
        desc:
            'Click to learn more about clinical trials going on around the world concerning COVID-19.',
        imageUrl: process.env.PUBLIC_URL + 'clinicaltrials.jpeg',
        href: '/ClinicalTrials',
    },
    {
        title: 'Global Data',
        desc:
            'Click to learn more about how countries around the world are doing at vaccinating their citizens.',
        imageUrl: process.env.PUBLIC_URL + 'global.jpeg',
        href: '/GlobalVacc',
    },
    {
        title: 'COVID Beliefs',
        desc:
            'Click to learn more about the varying beliefs surrounding COVID-19 vaccines around the world.',
        imageUrl: process.env.PUBLIC_URL + 'beliefs.jpeg',
        href: '/Beliefs',
    },
]

export default models
