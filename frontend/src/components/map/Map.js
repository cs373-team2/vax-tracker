import React from 'react'
import { MapContainer, TileLayer, CircleMarker } from 'react-leaflet'
import 'leaflet/dist/leaflet.css'

function Map(props) {
    var position = [props.latitude, props.longitude]
    return (
        <MapContainer
            style={{
                width: '600px',
                height: '600px',
                borderRadius: '25px 25px 0px 25px',
            }}
            center={position}
            zoom={4}
            scrollWheelZoom={false}
        >
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <CircleMarker
                center={position}
                pathOptions={{ color: 'red', fillColor: 'red' }}
                radius={30}
            ></CircleMarker>
        </MapContainer>
    )
}

export default Map
