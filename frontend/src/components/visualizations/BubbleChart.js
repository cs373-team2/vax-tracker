import React, { Component } from 'react'
import * as d3 from 'd3'
// import Error from '../ui/Error'
// import BubbleChartOrig from '@weknow/react-bubble-chart-d3'

/* Bubble chart visualization used to present ours and our provider's data */
class BubbleChart extends Component {
    constructor(props) {
        super(props)
        this.createBubbleChart = this.createBubbleChart.bind(this)
    }
    componentDidMount() {
        this.createBubbleChart()
    }

    componentDidUpdate() {
        this.createBubbleChart()
    }

    createBubbleChart() {
        let datamap = []
        for (let elem in this.props.data) {
            const obj = {
                Name: this.props.data[elem][this.props.xAttr],
                Count: this.props.data[elem][this.props.yAttr],
            }
            datamap.push(obj)
        }

        let dataset = { children: datamap }
        let diameter = 900
        let dataFix = d3
        let color = dataFix.scaleOrdinal(d3.schemeCategory10)
        let bubble = dataFix.pack(dataset).size([diameter, diameter])

        const svg = d3
            .select(this.refs.bubbleChart)
            .append('svg')
            .attr('width', diameter)
            .attr('height', diameter)
            .attr('id', 'NewGraph')
        let nodes = dataFix.hierarchy(dataset).sum(function (d) {
            return d.Count
        })
        let node = svg
            .selectAll('.node')
            .data(bubble(nodes).descendants())
            .enter()
            .filter(function (d) {
                return !d.children
            })
            .append('g')
            .attr('class', 'node')
            .attr('transform', function (d) {
                return 'translate(' + d.x + ',' + d.y + ')'
            })
        node.append('title').text(function (d) {
            return d.data.Name + ': ' + d.data.Count
        })
        node.append('circle')
            .attr('r', function (d) {
                return d.r
            })
            .style('fill', function (d, i) {
                return color(i)
            })

        node.append('text')
            .attr('dy', '.2em')
            .style('text-anchor', 'middle')
            .text(function (d) {
                return d.data.Name
            })
            .attr('font-family', 'sans-serif')
            .attr('font-size', function (d) {
                return d.r / 2.7
            })
            .attr('fill', 'black')

        node.append('text')
            .attr('dy', '1.3em')
            .style('text-anchor', 'middle')
            .text(function (d) {
                return d.data.Count
            })
            .attr('font-family', 'Gill Sans')
            .attr('font-size', function (d) {
                return d.r / 2.7
            })
            .attr('fill', 'black')
    }

    render() {
        return <div ref="bubbleChart"></div>
    }
}

export default BubbleChart
