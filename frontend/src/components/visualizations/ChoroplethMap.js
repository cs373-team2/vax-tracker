import React, { Component } from 'react'

class ChoroplethMap extends Component {
    constructor(props) {
        super(props)
        this.createWorldMap = this.createWorldMap.bind(this)
    }
    componentDidMount() {
        this.createWorldMap()
    }
    createWorldMap() {
        const WorldMap = require('react-svg-worldmap').WorldMap
        var data = []

        for (var i = 0; i < this.props.data.length; i++) {
            var cur1 = this.props.data[i][0]
            var cur2 = this.props.data[i][1]
            var dict = { country: cur1, value: cur2 }
            data.push(dict)
        }
        const stylingFunction = (context) => {
            const opacityLevel =
                0.1 +
                (6 * (context.countryValue - context.minValue)) /
                    (context.maxValue - context.minValue)
            return {
                fill: context.country === 'US' ? 'green' : context.color,
                fillOpacity: opacityLevel,
                stroke: 'green',
                strokeWidth: 1,
                strokeOpacity: 0.2,
                cursor: 'pointer',
            }
        }
        return (
            <div height="500px">
                <div id="root">
                    <WorldMap
                        color="green"
                        size="xxl"
                        data={data}
                        styleFunction={stylingFunction}
                    />
                </div>
            </div>
        )
    }

    render() {
        return this.createWorldMap()
    }
}

export default ChoroplethMap
