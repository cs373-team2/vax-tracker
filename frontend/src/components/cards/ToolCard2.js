import React from 'react'
import './ToolCard.css'

const ToolCard = ({ title, image, description, link, height }) => {
    return (
        <div>
            <div className="row">
                <div className="col-sm-4">
                    <div
                        className="card"
                        style={{
                            width: '13rem',
                            height: '18rem',
                            margin: '0.7rem 0.7rem 0.5rem 0.5rem',
                        }}
                    >
                        <img
                            className="card-img-top"
                            id="tool-img"
                            src={image}
                            alt=""
                        ></img>
                        <div className="card-body">
                            <h5 className="card-title">{title}</h5>
                            <p className="card-text">
                                <strong></strong>
                                {description}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ToolCard
