import React from 'react'

const StatsCard = ({ stat_title, stat, image }) => {
    return (
        <div>
            <div className="row">
                <div className="col-sm-4">
                    <div
                        className="card"
                        style={{
                            width: '13rem',
                            height: '7rem',
                            margin: '0.7rem 0.7rem 0.5rem 0.5rem',
                        }}
                    >
                        <div className="card-body">
                            <h5 className="card-title">{stat_title}</h5>
                            <h5 className="card-title">{stat}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default StatsCard
