import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        height: '20vw',
    },
    media: {
        height: 140,
    },
    title: {
        fontSize: 16,
    },
})

export default function NewsCard({ article }) {
    const classes = useStyles()
    // console.log(article.title)
    return (
        <Card className={classes.root}>
            <CardActionArea target="_blank" href={article.url}>
                <CardMedia
                    className={classes.media}
                    image={article.urlToImage}
                    title={article.title}
                />
                <CardContent>
                    <Typography
                        className={classes.title}
                        gutterBottom
                        variant="h4"
                        component="h2"
                    >
                        {article.title}
                    </Typography>
                    <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                    >
                        By {article.author}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    )
}
