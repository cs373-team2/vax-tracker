import React from 'react'
import './InfoCard.css'

const InfoCards = ({ members }) => {
    return (
        <div>
            <div className="row">
                <div className="col-sm-4">
                    <div
                        className="card"
                        style={{
                            width: '13rem',
                            height: '45rem',
                            margin: '0.3rem 0.3rem 0.3rem 0.3rem',
                        }}
                    >
                        <img
                            className="card-img-top"
                            alt=""
                            src={members.image}
                        ></img>
                        <div className="card-body">
                            <h5 className="card-title">{members.name}</h5>
                            <p className="card-text">{members.desc}</p>
                            <p className="card-text">
                                <strong>Role: </strong>
                                {members.jobs}
                            </p>
                            <p className="card-text">
                                <strong>Commits: </strong>
                                {members.commits}
                            </p>
                            <p className="card-text">
                                <strong>Issues: </strong>
                                {members.issues}
                            </p>
                            <p className="card-text">
                                <strong>Tests: </strong>
                                {members.tests}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default InfoCards
