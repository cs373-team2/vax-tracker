import React, { useState, useEffect } from 'react'
import './Navbar.css'
import 'bootstrap/dist/js/bootstrap.bundle.min'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSyringe } from '@fortawesome/free-solid-svg-icons'

export default function TopNavBar() {
    const [scrollState, setScrollState] = useState('top')

    useEffect(() => {
        let listener = null
        listener = document.addEventListener('scroll', (e) => {
            var scrolled = document.scrollingElement.scrollTop
            if (scrolled >= 75) {
                if (scrollState !== 'scrolled') {
                    setScrollState('scrolled')
                }
            } else {
                if (scrollState !== 'top') {
                    setScrollState('top')
                }
            }
        })
        return () => {
            document.removeEventListener('scroll', listener)
        }
    }, [scrollState])

    return (
        <Navbar
            className="mainNav"
            style={{
                backgroundColor:
                    scrollState === 'top' ? 'transparent' : '#93329e',
            }}
            fixed="top"
            expand="lg"
        >
            <Container fluid className="nav-container">
                <Navbar.Brand
                    style={{
                        fontFamily: 'Nunito',
                        color: scrollState === 'top' ? '#93329e' : '#fff',
                    }}
                    href="/"
                >
                    <span>
                        <FontAwesomeIcon
                            icon={faSyringe}
                            style={{
                                color:
                                    scrollState === 'top' ? '#93329e' : '#fff',
                            }}
                        ></FontAwesomeIcon>
                        VaxTracker
                    </span>
                </Navbar.Brand>
                <Navbar.Collapse
                    className="d-flex flex-row-reverse"
                    id="basic-navbar-nav"
                >
                    <Nav className="me-auto mb-2 mb-lg-0">
                        <Nav.Link
                            className="header-link"
                            href="/"
                            style={{
                                color:
                                    scrollState === 'top' ? '#93329e' : '#fff',
                            }}
                        >
                            Home
                        </Nav.Link>
                        <Nav.Link
                            className="header-link"
                            href="/about"
                            style={{
                                color:
                                    scrollState === 'top' ? '#93329e' : '#fff',
                            }}
                        >
                            About
                        </Nav.Link>
                        <Nav.Link
                            className="header-link"
                            href="/GlobalVacc"
                            style={{
                                color:
                                    scrollState === 'top' ? '#93329e' : '#fff',
                            }}
                        >
                            Global Data
                        </Nav.Link>
                        <Nav.Link
                            className="header-link"
                            href="/ClinicalTrials"
                            style={{
                                color:
                                    scrollState === 'top' ? '#93329e' : '#fff',
                            }}
                        >
                            Clinical Trials
                        </Nav.Link>
                        <Nav.Link
                            className="header-link"
                            href="/Beliefs"
                            style={{
                                color:
                                    scrollState === 'top' ? '#93329e' : '#fff',
                            }}
                        >
                            COVID Belief Data
                        </Nav.Link>
                        <Nav.Link
                            className="header-link"
                            href="/Vaccines"
                            style={{
                                color:
                                    scrollState === 'top' ? '#93329e' : '#fff',
                            }}
                        >
                            COVID Vaccine Info
                        </Nav.Link>
                        <NavDropdown
                            title="Visualizations"
                            id={
                                scrollState === 'top'
                                    ? 'nav-text-color'
                                    : 'nav-text-light'
                            }
                        >
                            <Nav.Link
                                className="header-link"
                                href="/OurVisualizations"
                            >
                                Our Visualizations
                            </Nav.Link>
                            <Nav.Link
                                className="header-link"
                                href="/ProviderVisualizations"
                            >
                                Provider Visualizations
                            </Nav.Link>
                        </NavDropdown>
                        <Nav.Link
                            className="header-link"
                            href="/search"
                            style={{
                                color:
                                    scrollState === 'top' ? '#93329e' : '#fff',
                            }}
                        >
                            Search
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
