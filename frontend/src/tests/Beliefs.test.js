import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import * as Enzyme from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import { shallow } from 'enzyme'
// import { BrowserRouter as Router } from 'react-router-dom'

import Beliefs from '../pages/beliefs/Beliefs.js'
Enzyme.configure({
    adapter: new Adapter(),
})

it('Beliefs Page', () => {
    const copy = shallow(<Beliefs />)
    expect(copy).not.toBeNull()
    expect(copy).toBeDefined()
})
