import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import * as Enzyme from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import { shallow } from 'enzyme'
// import { BrowserRouter as Router } from 'react-router-dom'

import Search from '../pages/search/Search.js'
Enzyme.configure({
    adapter: new Adapter(),
})

it('About Page', () => {
    const copy = shallow(<Search />)
    expect(copy).not.toBeNull()
    expect(copy).toBeDefined()
})
