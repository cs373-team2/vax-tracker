import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import * as Enzyme from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import { shallow } from 'enzyme'
// import { BrowserRouter as Router } from 'react-router-dom'

import ClinicalTrials from '../pages/clinical-trials/ClinicalTrials.js'
Enzyme.configure({
    adapter: new Adapter(),
})

it('ClinicalTrials Page', () => {
    const copy = shallow(<ClinicalTrials />)
    expect(copy).not.toBeNull()
    expect(copy).toBeDefined()
})
