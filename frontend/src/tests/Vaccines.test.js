import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import * as Enzyme from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import { shallow } from 'enzyme'
// import { BrowserRouter as Router } from 'react-router-dom'

import Vaccines from '../pages/vaccines/Vaccines.js'
Enzyme.configure({
    adapter: new Adapter(),
})

it('Vaccines Page', () => {
    const copy = shallow(<Vaccines />)
    expect(copy).not.toBeNull()
    expect(copy).toBeDefined()
})
