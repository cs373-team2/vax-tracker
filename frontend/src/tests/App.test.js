import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import * as Enzyme from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import { shallow } from 'enzyme'

import App from '../App.js'

Enzyme.configure({
    adapter: new Adapter(),
})

it('App Page', async () => {
    const copy = shallow(<App />)
    expect(copy).not.toBeNull()
})
