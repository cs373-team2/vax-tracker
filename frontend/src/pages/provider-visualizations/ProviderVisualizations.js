import React from 'react'
import BarChart from '../../components/visualizations/BarChart'
import BubbleChart from '../../components/visualizations/BubbleChart'
import ChoroplethMap from '../../components/visualizations/ChoroplethMap'
import attractions from '../../vizdata/attractions.json'
import cities from '../../vizdata/cities.json'
import hotels from '../../vizdata/country_hotel_ratings.json'
import PropTypes from 'prop-types'
import SwipeableViews from 'react-swipeable-views'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'

function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    )
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
}

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    }
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        minHeight: '900px',
        margin: 'auto',
        paddingTop: '70px',
        width: '80%',
    },
}))

const ProviderVisualizations = () => {
    const classes = useStyles()
    const theme = useTheme()
    const [value, setValue] = React.useState(0)

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    const handleChangeIndex = (index) => {
        setValue(index)
    }

    return (
        <div className={classes.root}>
            <h1>Provider Visualizations</h1>
            <a
                target="_blank"
                rel="noreferrer"
                href="https://www.travvyexplorers.com/"
            >
                Travvy Explorers
            </a>
            <p></p>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                >
                    <Tab label="Cities" {...a11yProps(0)} />
                    <Tab label="Attractions" {...a11yProps(1)} />
                    <Tab label="Hotels" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <h2>Average Hotel Rating by Country</h2>
                    <ChoroplethMap data={hotels} />
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    <h2>Number of Attractions per Type of Attraction</h2>
                    <BarChart
                        data={attractions}
                        xAttr="category"
                        yAttr="num_attractions"
                        xLabel="Categories"
                        yLabel="Number of Attractions"
                    />
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                    <h2>Number of Hotels per City</h2>
                    <BubbleChart
                        data={cities}
                        xAttr="city_name"
                        yAttr="num_hotels"
                    />
                </TabPanel>
            </SwipeableViews>
        </div>
    )
}

export default ProviderVisualizations
