import React from 'react'
import renderer from 'react-test-renderer'
import CountryGrid from './CountryGrid'
import { mockCountry } from './CountryCard.test'

test('CountryGrid Component Renders', () => {
    const six = [].fill(mockCountry(), 6)
    const three = [].fill(mockCountry(), 3)
    let component = renderer.create(<CountryGrid data={six}></CountryGrid>)
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
    component = renderer.create(<CountryGrid data={three}></CountryGrid>)
    tree = component.toJSON()
    expect(tree).toMatchSnapshot()
})
