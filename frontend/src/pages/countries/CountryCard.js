import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { CardMedia } from '@material-ui/core'
import Card from '@material-ui/core/Card'
// import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
// import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import { thousands_separators } from './GlobalVacc'
import './CountryCard.css'

import Highlighter from 'react-highlight-words'

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        height: 170,
    },
})

function returnString(data) {
    if (data === undefined) {
        return 'N/A'
    } else {
        return data.toString()
    }
}

export default function CountryCard({ data, searchQuery }) {
    const classes = useStyles()
    return (
        <a href={'/globalvacc/' + data.country_id}>
            <Card className={classes.root} variant="outlined">
                <CardContent>
                    <CardMedia
                        className={classes.media}
                        image={data.flag}
                        title="flag"
                    />
                    <Typography
                        className={classes.pos}
                        variant="h6"
                        component="h2"
                        align="center"
                    >
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={data.name}
                        />
                    </Typography>
                    <Typography className={classes.pos}>
                        Capital:{' '}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={data.capital}
                        />
                    </Typography>
                    <Typography className={classes.pos}>
                        Population:{' '}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={thousands_separators(
                                data.population
                            )}
                        />
                    </Typography>
                    <Typography className={classes.pos}>
                        Number of Vaccinations:{' '}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={thousands_separators(
                                data.total_doses_admin
                            )}
                        />
                    </Typography>
                    <Typography className={classes.pos}>
                        Number of People Vaccinated:{' '}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={thousands_separators(
                                data.total_people_vacc
                            )}
                        />
                    </Typography>
                    <Typography className={classes.pos}>
                        Total Vaccinations Per 100:{' '}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={returnString(
                                data.total_vaccinations_per100
                            )}
                        />
                    </Typography>
                    <Typography className={classes.pos}>
                        Number of Approved Vaccines:{' '}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={returnString(
                                data.approved_vaccines
                            )}
                        />
                    </Typography>
                </CardContent>
            </Card>
        </a>
    )
}
