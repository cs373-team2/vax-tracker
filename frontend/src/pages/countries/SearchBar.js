import React from 'react'

const SearchBar = ({ searchQuery: keyword, onChange: setKeyword }) => {
    const BarStyling = {
        width: '100%',
        borderRadius: '4 px',
        background: '#F2F1F9',
        border: 'none',
        padding: '0.5rem',
    }
    return (
        <input
            style={BarStyling}
            key="random1"
            value={keyword}
            placeholder={'Search for a country'}
            onChange={(e) => setKeyword(e.target.value)}
        />
    )
}

export default SearchBar
