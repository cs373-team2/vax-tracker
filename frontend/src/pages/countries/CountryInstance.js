import React from 'react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { latlon } from '../../components/map/latlon.js'
import Map from '../../components/map/Map'
import CountUp from 'react-countup'
import './CountryInstance.css'

const CountryInstance = ({ match }) => {
    const [country, setCountry] = useState([])
    const [trials, setTrials] = useState([])
    const [vaccines, setVaccines] = useState([])
    const setRelationshipData = (data, field) => {
        let arr = []
        for (let i = 0; i < Object.keys(data).length; i++) {
            arr.push(data[i][field])
        }
        let id_list = arr.join()
        return id_list
    }

    useEffect(() => {
        // generic async JSON fetching code
        async function fetchJSON(url) {
            var res = await fetch(url)
            return res.json()
        }
        async function fetchCountry(id) {
            let countryEntry = await fetchJSON(
                'https://vaxtracker.me/api/countries?countryId=' + id
            )
            setCountry(countryEntry[0])
            let trialLinks = await fetchJSON(
                'https://vaxtracker.me/api/trial_link?countryId=' + id
            )
            if (trialLinks.length) {
                let trialIdList = setRelationshipData(trialLinks, 'trial_id')
                let trials = await fetchJSON(
                    'https://vaxtracker.me/api/trials?trialId=' + trialIdList
                )
                setTrials(trials)
            }
            let vaccineLinks = await fetchJSON(
                'https://vaxtracker.me/api/vaccine_link?countryId=' + id
            )
            if (vaccineLinks.length) {
                let vaccineIdList = setRelationshipData(vaccineLinks, 'vaccine_id')
                let vaccines = await fetchJSON(
                    'https://vaxtracker.me/api/vaccines?vaccineId=' + vaccineIdList
                )
                setVaccines(vaccines)
            }
        }
        fetchCountry(match.params.id)
    }, [match.params.id])

    return (
        <>
            {latlon[country.country_id - 1] !== undefined && (
                <div class="container country--page">
                    <div class="row border">
                        <div class="col-xs-4">
                            <h2 class="country--name">{country.name}</h2>
                        </div>
                        <div class="col-xs-8">
                            <h2 class="capital--name">{country.capital}</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 num--vacc border">
                            <h2>
                                <CountUp
                                    end={country.total_people_vacc}
                                    separator=","
                                    suffix={
                                        ' (' +
                                        parseFloat(
                                            country.percent_pop_vacc
                                        ).toFixed(2) +
                                        '%)'
                                    }
                                ></CountUp>
                            </h2>
                            <h4 class="o50">Fully Vaccinated</h4>
                        </div>
                        <div class="col-sm-4 num--vacc border">
                            <h2>
                                <CountUp
                                    end={country.total_active_cases}
                                    separator=","
                                ></CountUp>
                            </h2>
                            <h4 class="o50">Active Cases</h4>
                        </div>
                        <div class="col-sm-4 num--vacc border">
                            <h2>
                                <CountUp
                                    end={country.population}
                                    separator=","
                                ></CountUp>
                            </h2>
                            <h4 class="o50">Population</h4>
                        </div>

                        <div class="col-sm-4 num--vacc border">
                            <h2>{country.iso2}</h2>
                            <h4 class="o50">ISO 2</h4>
                        </div>
                        <div class="col-sm-4 num--vacc border">
                            <h2>
                                <CountUp
                                    end={country.gdp}
                                    separator=","
                                ></CountUp>
                            </h2>
                            <h4 class="o50">GDP</h4>
                        </div>
                        <div class="col-sm-4 num--vacc border">
                            <h2>
                                <CountUp
                                    end={country.total_doses_admin}
                                    separator=","
                                ></CountUp>
                            </h2>
                            <h4 class="o50">Total Doses Administered</h4>
                        </div>
                        <div class="col-sm-4 num--vacc border">
                            <h2>
                                <CountUp
                                    end={country.approved_vaccines}
                                    separator=","
                                ></CountUp>
                            </h2>
                            <h4 class="o50">Number of Vaccines Approved</h4>
                        </div>
                        <div class="col-sm-4 num--vacc border">
                            <h2>
                                <CountUp
                                    end={country.persons_vaccinated_1plus_dose}
                                    separator=","
                                ></CountUp>
                            </h2>
                            <h4 class="o50">
                                Number of People with 1 or more doses
                            </h4>
                        </div>
                        <div class="col-sm-4 num--vacc border">
                            <h2>{country.total_vaccinations_per100}</h2>
                            <h4 class="o50">
                                Number of Vaccines per 100 People
                            </h4>
                        </div>
                    </div>
                    <div class="row country--visual border">
                        <div class="col-sm-6 flag">
                            <Map
                                longitude={
                                    latlon[country.country_id - 1].longitude
                                }
                                latitude={
                                    latlon[country.country_id - 1].latitude
                                }
                                style={{ width: '100%', 'border-radius': '0' }}
                            />
                        </div>
                        <div class="col-sm-6 map">
                            <img alt="" src={country.flag} />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 sublabel border">
                            <h3>Approved Vaccines</h3>
                        </div>
                        <div class="col-sm-6 sublabel border">
                            <h3>Clinical Trials</h3>
                        </div>
                    </div>
                    <div class="row border">
                        <div class="col-sm-6 col-reset mtable">
                            <div class="card entry">
                                <ul class="list-group list-group-flush">
                                    {vaccines.length > 0 &&
                                        vaccines.map((vaccine) => (
                                            <li className="list-group-item">
                                                <Link
                                                    to={
                                                        '/Vaccines/' +
                                                        vaccine.vaccine_id
                                                    }
                                                >
                                                    {vaccine.name}
                                                </Link>
                                            </li>
                                        ))}
                                    {vaccines.length === 0 && (
                                        <li className="list-group-item">
                                            Nothing here yet
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6 col-reset mtable">
                            <div class="card entry">
                                <ul class="list-group list-group-flush">
                                    {trials.length > 0 &&
                                        trials.map((trial) => (
                                            <li className="list-group-item">
                                                <Link
                                                    to={
                                                        '/ClinicalTrials/' +
                                                        trial.trial_id
                                                    }
                                                >
                                                    {trial.title}
                                                </Link>
                                            </li>
                                        ))}
                                    {trials.length === 0 && (
                                        <li className="list-group-item">
                                            Nothing here yet
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}
export default CountryInstance
