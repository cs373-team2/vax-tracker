import React from 'react'
import renderer from 'react-test-renderer'
import CountryCard from './CountryCard'

export const mockCountry = () => {
    return {
        country_id: 1,
        name: 'Afghanistan',
        iso2: 'AF',
        population: 37172386,
        capital: 'Kabul',
        flag:
            'https://upload.wikimedia.org/wikipedia/commons/9/9a/Flag_of_Afghanistan.svg',
        gdp: '20536542736.7297',
        total_doses_admin: '54000',
        total_people_vacc: '54000.0',
        percent_pop_vacc: '0.1452691253125371',
        approved_vaccines: '0',
        total_active_cases: '3924',
    }
}

test('CountryCard Component Renders', () => {
    const component = renderer.create(
        <CountryCard data={mockCountry()}></CountryCard>
    )
    let tree = component.toJSON()
    expect(tree).toMatchSnapshot()
})
