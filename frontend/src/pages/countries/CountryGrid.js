import React from 'react'
import CountryCard from './CountryCard'
import { Grid } from '@material-ui/core'

function CountryGrid({ data, searchQuery }) {
    return (
        <Grid container spacing={3} justify="center">
            {data.map((col) => {
                return (
                    <Grid item key={col.country_id} xs={4}>
                        <CountryCard data={col} searchQuery={searchQuery} />
                    </Grid>
                )
            })}
        </Grid>
    )
}
export default CountryGrid
