import React from 'react'
import { useEffect, useState } from 'react'
import './GlobalVacc.css'
import { Pagination } from '@material-ui/lab'
import CountryGrid from './CountryGrid'
import { makeStyles } from '@material-ui/core/styles'
import Select from 'react-select'

import SearchBar from './SearchBar.js'

const useStyles = makeStyles((theme) => ({
    pagination: {
        justifyContent: 'center',
        display: 'flex',
        '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}))

export function thousands_separators(num) {
    if (num === undefined) return
    var num_parts = num.toString().split('.')
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    return num_parts.join('.')
}

function GlobalVacc() {
    const CardsPerPage = 9
    const styles = useStyles()
    const filters = [
        {
            name: 'None',
            func: (a) => {
                return true
            },
        },
        {
            name: '0 < Population <= 1,000,00',
            func: (a) => {
                return a.population > 0 && a.population <= 1000000
            },
        },
        {
            name: '1,000,000 < Population <= 10,000,000',
            func: (a) => {
                return a.population > 1000000 && a.population <= 10000000
            },
        },
        {
            name: '10,000,000 < Population <= 50,000,000',
            func: (a) => {
                return a.population > 10_000_000 && a.population <= 50_000_000
            },
        },
        {
            name: '50,000,000 < Population <= 100,000,000',
            func: (a) => {
                return a.population > 50_000_000 && a.population <= 100_000_000
            },
        },
        {
            name: '100,000,000 < Population <= 500,000,000',
            func: (a) => {
                return a.population > 100_000_000 && a.population <= 500_000_000
            },
        },
        {
            name: 'Population > 500,000,000',
            func: (a) => {
                return a.population > 500_000_000
            },
        },
        {
            name: '0 < Vaccinations <= 1,000,00',
            func: (a) => {
                return a.total_people_vacc > 0 && a.total_people_vacc <= 1000000
            },
        },
        {
            name: '1,000,000 < Vaccinations <= 10,000,000',
            func: (a) => {
                return (
                    a.total_people_vacc > 1000000 &&
                    a.total_people_vacc <= 10000000
                )
            },
        },
        {
            name: '10,000,000 < Vaccinations <= 50,000,000',
            func: (a) => {
                return (
                    a.total_people_vacc > 10_000_000 &&
                    a.total_people_vacc <= 50_000_000
                )
            },
        },
        {
            name: '50,000,000 < Vaccinations <= 100,000,000',
            func: (a) => {
                return (
                    a.total_people_vacc > 50_000_000 &&
                    a.total_people_vacc <= 100_000_000
                )
            },
        },
        {
            name: '100,000,000 < Vaccinations <= 500,000,000',
            func: (a) => {
                return (
                    a.total_people_vacc > 100_000_000 &&
                    a.total_people_vacc <= 500_000_000
                )
            },
        },
        {
            name: 'Vaccinations > 500,000,000',
            func: (a) => {
                return a.total_people_vacc > 500_000_000
            },
        },
        {
            name: '0 < Approved Vaccines <= 1',
            func: (a) => {
                return a.approved_vaccines >= 0 && a.approved_vaccines <= 1
            },
        },
        {
            name: '1 < Approved Vaccines <= 2',
            func: (a) => {
                return a.approved_vaccines > 1 && a.approved_vaccines <= 2
            },
        },
        {
            name: '2 < Approved Vaccines <= 5',
            func: (a) => {
                return a.approved_vaccines > 2 && a.approved_vaccines <= 5
            },
        },
        {
            name: '5 < Approved Vaccines <= 10',
            func: (a) => {
                return a.approved_vaccines > 5 && a.approved_vaccines <= 10
            },
        },
        {
            name: 'Approved Vaccines > 10',
            func: (a) => {
                return a.approved_vaccines > 10
            },
        },
        {
            name: '0-25 Vaccinations per 100',
            func: (a) => {
                return (
                    a.total_vaccinations_per100 >= 0 &&
                    a.total_vaccinations_per100 <= 25
                )
            },
        },
        {
            name: '26-50 Vaccinations per 100',
            func: (a) => {
                return (
                    a.total_vaccinations_per100 > 25 &&
                    a.total_vaccinations_per100 <= 50
                )
            },
        },
        {
            name: '51-75 Vaccinations per 100',
            func: (a) => {
                return (
                    a.total_vaccinations_per100 > 50 &&
                    a.total_vaccinations_per100 <= 75
                )
            },
        },
        {
            name: '76-100 Vaccinations per 100',
            func: (a) => {
                return (
                    a.total_vaccinations_per100 > 75 &&
                    a.total_vaccinations_per100 <= 100
                )
            },
        },
    ]
    const sorts = [
        {
            name: 'Alphabetical',
            func: (a, b) => {
                return a.name.localeCompare(b.name)
            },
        },
        {
            name: 'Population (low to high)',
            func: (a, b) => {
                return a.population - b.population
            },
        },
        {
            name: 'Population (high to low)',
            func: (a, b) => {
                return b.population - a.population
            },
        },
        {
            name: 'Alphabetical by Capital City',
            func: (a, b) => {
                return a.capital.localeCompare(b.capital)
            },
        },
        {
            name: 'Vaccinations (low to high)',
            func: (a, b) => {
                return a.total_people_vacc - b.total_people_vacc
            },
        },
        {
            name: 'Vaccinations (high to low)',
            func: (a, b) => {
                return b.total_people_vacc - a.total_people_vacc
            },
        },
        {
            name: 'Approved Vaccines (low to high)',
            func: (a, b) => {
                return a.approved_vaccines - b.approved_vaccines
            },
        },
        {
            name: 'Approved Vaccines (high to low)',
            func: (a, b) => {
                return b.approved_vaccines - a.approved_vaccines
            },
        },
        {
            name: 'Active Cases (high to low)',
            func: (a, b) => {
                return b.total_active_cases - a.total_active_cases
            },
        },
        {
            name: 'Active Cases (low to high)',
            func: (a, b) => {
                return a.total_active_cases - b.total_active_cases
            },
        },
        {
            name: 'GDP (low to high)',
            func: (a, b) => {
                return a.gdp - b.gdp
            },
        },
        {
            name: 'GDP (high to low)',
            func: (a, b) => {
                return b.gdp - a.gdp
            },
        },
    ]
    var [countries, setCountries] = useState([])
    var [currentData, setCurrentData] = useState([])
    var [activeFilter, setActiveFilter] = useState(0);
    var [activeSort, setActiveSort] = useState(0);

    var [searchData, setSearchData] = useState([])
    var [searchQuery, setSeachQuery] = useState('')

    async function updateSearchQuery(searchQuery) {
        var filtered = countries;
        if (searchQuery !== '') {
            filtered = countries.filter((data) => {
                var cur = data.name
                    .toLowerCase()
                    .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    thousands_separators(data.population)
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.capital
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    thousands_separators(data.total_doses_admin)
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    thousands_separators(data.total_people_vacc)
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.total_vaccinations_per100
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.approved_vaccines
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                return cur
            })
        }

        setSeachQuery(searchQuery)
        setSearchData(filtered.filter(filters[activeFilter].func).sort(sorts[activeSort].func))
        setCurrentData(searchData.slice(0, CardsPerPage))
    }
    // TODO: implement server-side pageination
    // fetch country data and render on callback
    useEffect(() => {
        const url = 'https://vaxtracker.me/api/countries'
        fetchJSON(url).then((data) => {
            setCountries(data)
            setSearchData(data)
            setCurrentData(data.slice(0, CardsPerPage))
        })
    }, [])

    // generic async JSON fetching code
    async function fetchJSON(url) {
        var res = await fetch(url)
        return res.json()
    }

    async function sortBy(option) {
        setActiveSort(option.value)
        setSearchData(searchData.sort(sorts[option.value].func))
        setCurrentData(searchData.slice(0, CardsPerPage))
    }

    async function filterBy(option) {
        setActiveFilter(option.value)
        // console.log(option)
        let filtered = searchData.filter(filters[option.value].func)
        setSearchData(filtered)
        setCurrentData(filtered.slice(0, CardsPerPage))
    }

    // update current "data frame"
    function handleChangePage(pageNumber) {
        setCurrentData(
            searchData.slice(
                pageNumber * CardsPerPage - CardsPerPage,
                pageNumber * CardsPerPage
            )
        )
    }

    var numTotalPages = Math.ceil(searchData.length / CardsPerPage)

    return (
        <div className="container" style={{ paddingTop: '70px' }}>
            <h2>Global Vaccine Data</h2>
            <h5>
                Click on a country to see its demographic information as well as
                its Covid-19 statistics.
            </h5>
            <Select
                placeholder="Sort by..."
                options={sorts.map((e, i) => {
                    return { value: i, label: e.name }
                })}
                onChange={sortBy}
            />
            <Select
                placeholder="Filter by..."
                options={filters.map((e, i) => {
                    return { value: i, label: e.name }
                })}
                onChange={filterBy}
            />
            <SearchBar searchQuery={searchQuery} onChange={updateSearchQuery} />
            <br></br>
            <br></br>

            <CountryGrid
                data={currentData}
                searchQuery={searchQuery}
            ></CountryGrid>
            <div className={styles.pagination}>
                <Pagination
                    count={numTotalPages}
                    onChange={(_, page) => handleChangePage(page)}
                    showFirstButton
                    showLastButton
                />
                <h5>Total Countries: {searchData.length}</h5>
            </div>
        </div>
    )
}

export default GlobalVacc
