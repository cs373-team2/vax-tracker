import React, { useState, useEffect } from 'react'
import { memberInfo } from '../../static/MemberInfo'
import { toolsInfo } from '../../static/ToolsInfo'
import { data_api_info } from '../../static/DataAPIInfo'
import { projectInfo } from '../../static/ProjectInfo'
import InfoCards from '../../components/cards/InfoCards'
import ToolCard from '../../components/cards/ToolCard'
import ToolCard2 from '../../components/cards/ToolCard2'
import StatsCard from '../../components/cards/StatsCard'
import Spinner from '../../components/ui/Spinner'
import './About.css'

const getGitlabInfo = async () => {
    let totalCommitCount = 0,
        totalIssueCount = 0,
        totalTestCount = 0
    memberInfo.forEach((member) => {
        totalTestCount += member.tests
        member.issues = 0
        member.commits = 0
    })
    let page = 1
    let commitPage = []
    let commitList = []
    do {
        commitPage = await fetch(
            'https://gitlab.com/api/v4/projects/24677261/repository/commits?per_page=400&page=' +
                page
        )
        commitPage = await commitPage.json()
        commitList = [...commitList, ...commitPage]
        page++
    } while (commitPage.length === 400)

    commitList.forEach((element) => {
        memberInfo.forEach((member) => {
            if (member.email === element.author_email) {
                member.commits += 1
            }
        })
        totalCommitCount += 1
    })
    page = 1
    let issuePage = []
    let issueList = []
    do {
        issuePage = await fetch(
            'https://gitlab.com/api/v4/projects/24677261/issues?per_page=100&page=' +
                page
        )
        issuePage = await issuePage.json()
        issueList = [...issueList, ...issuePage]
        page++
    } while (issuePage.length === 100)

    issueList.forEach((element) => {
        memberInfo.forEach((member) => {
            if (member.gitlab === element.author.username) {
                member.issues += 1
            }
        })
        totalIssueCount += 1
    })
    return {
        totalCommits: totalCommitCount,
        totalIssues: totalIssueCount,
        totalTests: totalTestCount,
        memberInfo: memberInfo,
    }
}

const About = () => {
    const [members, setMembers] = useState([])
    const [totalCommits, setTotalCommits] = useState(0)
    const [totalIssues, setTotalIssues] = useState(0)
    const [totalTests, setTotalTests] = useState(0)
    const [loaded, setLoaded] = useState(false)

    useEffect(() => {
        const fetchData = async () => {
            if (members === undefined || members.length === 0) {
                const gitlabInfo = await getGitlabInfo()
                setTotalCommits(gitlabInfo.totalCommits)
                setTotalIssues(gitlabInfo.totalIssues)
                setTotalTests(gitlabInfo.totalTests)
                setMembers(gitlabInfo.memberInfo)
                setLoaded(true)
            }
        }
        fetchData()
    }, [members])

    return (
        <div
            style={{
                paddingTop: '70px',
                paddingBottom: '30px',
                paddingLeft: '5%',
                paddingRight: '5%',
            }}
        >
            <h1>Welcome to Vax Tracker!</h1>
            <h5 className="website_description">
                Vax Tracker is a website that allows users to find data about
                Covid-19 vaccinations quickly and easily. We have data on
                global vaccination data, clinical trials, beliefs, and vaccine information
                related to Covid-19.{' '}
            </h5>
            <br></br>
            <h1>Our Team</h1>
            {loaded ? (
                <div className="row justify-content-center">
                    <InfoCards members={members[0]} />
                    <InfoCards members={members[1]} />
                    <InfoCards members={members[2]} />
                    <InfoCards members={members[3]} />
                    <InfoCards members={members[4]} />
                </div>
            ) : (
                <Spinner />
            )}
            <br></br>
            <h1>Repository Information</h1>
            <div className="row justify-content-center">
                <StatsCard stat_title={'Commits'} stat={totalCommits} />
                <StatsCard stat_title={'Issues'} stat={totalIssues} />
                <StatsCard stat_title={'Tests'} stat={totalTests} />
            </div>
            <br></br>

            <h1>API and Data Used</h1>
            <p>
                The data is mainly scraped from the following APIs with some
                additional data from the following supplementary link.{' '}
            </p>
            <div className="row justify-content-center">
                {data_api_info.map(({ title, image, description, link }) => {
                    return (
                        <a
                            href={link}
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            <ToolCard2
                                class="cards"
                                title={title}
                                image={image}
                                link={link}
                            />
                        </a>
                    )
                })}
            </div>
            <br></br>

            <h1>Tools Used</h1>
            <div className="row justify-content-center">
                {toolsInfo.map(({ title, image, description, link }) => {
                    return (
                        <a
                            href={link}
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            <ToolCard
                                title={title}
                                image={image}
                                description={description}
                                link={link}
                                style={{ height: '23rem' }}
                            />
                        </a>
                    )
                })}
            </div>
            <br></br>

            <h1>VaxTracker GitLab Respository, Postman API, & Video Presentation</h1>
            <div className="row justify-content-center">
                {projectInfo.map(({ title, image, link }) => {
                    return (
                        <a
                            href={link}
                            rel="noopener noreferrer"
                            target="_blank"
                        >
                            <ToolCard2
                                title={title}
                                image={image}
                                link={link}
                            />
                        </a>
                    )
                })}
            </div>
        </div>
    )
}

export default About
