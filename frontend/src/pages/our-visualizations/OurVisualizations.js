import React from 'react'
import BarChart from '../../components/visualizations/BarChart'
import BubbleChart from '../../components/visualizations/BubbleChart'
import beliefs from '../../vizdata/beliefs_data.json'
import vaccines from '../../vizdata/vaccines.json'
import global from '../../vizdata/global_vacc.json'
import ChoroplethMap from '../../components/visualizations/ChoroplethMap'
import PropTypes from 'prop-types'
import SwipeableViews from 'react-swipeable-views'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography'
import Box from '@material-ui/core/Box'
// import { createMuiTheme } from '@material-ui/core/styles';
// import purple from '@material-ui/core/colors/purple';

function TabPanel(props) {
    const { children, value, index, ...other } = props

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    )
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
}

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    }
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        minHeight: '900px',
        margin: 'auto',
        paddingTop: '70px',
        width: '80%',
    },
}))

const OurVisualizations = () => {
    const classes = useStyles()
    const theme = useTheme()
    const [value, setValue] = React.useState(0)

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    const handleChangeIndex = (index) => {
        setValue(index)
    }

    return (
        <div className={classes.root}>
            <h1>Our Visualizations</h1>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                >
                    <Tab label="Global Data" {...a11yProps(0)} />
                    <Tab label="Beliefs" {...a11yProps(1)} />
                    <Tab label="Vaccines" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <h2>
                        Total COVID-19 Vaccinations per 100 people per Country
                    </h2>
                    <ChoroplethMap data={global} />
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    <h2>
                        Percentage of Survey Participants Who Would Take
                        COVID-19 Vaccine per Country
                    </h2>
                    <BarChart
                        data={beliefs}
                        xAttr="country"
                        yAttr="perc_take_vaccine"
                        xLabel="Countries"
                        yLabel="Percentage of Participants Who Would Take Vaccine"
                    />
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                    <h2>Number of COVID-19 Vaccines per Trial Phase</h2>
                    <BubbleChart
                        data={vaccines}
                        xAttr="trial_phase"
                        yAttr="num_vaccines"
                    />
                </TabPanel>
            </SwipeableViews>
        </div>
    )
}

export default OurVisualizations
