import React from 'react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Grid } from '@material-ui/core/'
import { makeStyles } from '@material-ui/core/styles'
import NewsCard from '../../components/cards/NewsCard'
// import Spinner from '../../components/ui/Spinner'
import './ClinicalTrialInstance.css'

const useStyles = makeStyles((theme) => ({
    cards: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
}))

const ClinicalTrialsInstance = ({ match }) => {
    const classes = useStyles()
    const [trial, setTrial] = useState([])
    const [countries, setCountries] = useState([])
    const [beliefs, setBeliefs] = useState([])
    // const [status, setStatus] = useState(false)

    const setRelationshipData = (data, field) => {
        // console.log(data)
        let arr = []
        for (let i = 0; i < Object.keys(data).length; i++) {
            arr.push(data[i][field])
        }
        if (arr) {
            let id_list = arr.join()
            return id_list
        } else {
            return 'None'
        }
    }

    useEffect(() => {
        // generic async JSON fetching code
        async function fetchJSON(url) {
            var res = await fetch(url)
            return res.json()
        }
        async function fetchTrial(id) {
            let trialEntry = await fetchJSON(
                'https://vaxtracker.me/api/trials?trialId=' + id
            )
            setTrial(trialEntry[0])
            let countrylinks = await fetchJSON(
                'https://vaxtracker.me/api/trial_link?trialId=' + id
            )
            let countryidlist = setRelationshipData(countrylinks, 'country_id')
            let countrylist = []
            if (countryidlist.localeCompare('None')) {
                countrylist = await fetchJSON(
                    'https://vaxtracker.me/api/countries?countryId=' + countryidlist
                )
            }
            setCountries(countrylist)
            // console.log(countryidlist)
            let belieflinks = await fetchJSON(
                'https://vaxtracker.me/api/belief_link?countryId=' + countryidlist
            )
            // console.log(belieflinks)
            let beliefidlist = setRelationshipData(belieflinks, 'belief_id')
            // console.log(beliefidlist)
            let belieflist = []
            if (beliefidlist.localeCompare('None')) {
                belieflist = await fetchJSON(
                    'https://vaxtracker.me/api/beliefs?beliefId=' + beliefidlist
                )
            }
            setBeliefs(belieflist)
            // console.log(beliefs)
            // setstatus(true)
        }
        fetchTrial(match.params.id)
    }, [match.params.id])


    function displayCountryLinks(countries) {
        // console.log(countries)
        if (countries.length) {
            return (
                <div>
                    {countries.map((country) => (
                        <div>
                            <Link to={'/globalvacc/' + country.country_id}>
                                {country.name}
                            </Link>
                            <br />
                        </div>
                    ))}
                </div>
            )
        } else {
            return <p>None</p>
        }
    }

    function displayBeliefLinks(beliefs) {
        // console.log(beliefs)
        if (beliefs.length) {
            return (
                <div>
                    {beliefs.map((belief) => (
                        <div>
                            <Link to={'/beliefs/' + belief.id}>
                                {belief.country_name}
                            </Link>
                            <br />
                        </div>
                    ))}
                </div>
            )
        } else {
            return <p>None</p>
        }
    }

    function displayNewsArticles(articles) {
        var present = false
        if (articles) {
            var articlesJSON = JSON.parse(articles)
            // console.log(articlesJSON)
            present = true
        }
        if (present) {
            return (
                <div className={classes.cards}>
                    <Grid
                        container
                        spacing={2}
                        direction="row"
                        justify="center"
                        alignItems="flex-start"
                    >
                        {articlesJSON.map((article) => (
                            <Grid item key={articlesJSON.indexOf(article)}>
                                <NewsCard article={article} />
                            </Grid>
                        ))}
                    </Grid>
                </div>
            )
        } else {
            return <p>No news on {trial.title}</p>
        }
    }

    // if (!status) return <Spinner />
    return (
        <div class="container country--page2">
            <div class="row border">
                <div class="col-sm-6 map">
                    <h2 class="country--name">{trial.title}</h2>
                </div>
                <div class="col-sm-6 map">
                    <h2 class="capital--name">{trial.status}</h2>
                    {/* <h4 class="o50">Trial Status</h4> */}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.study_type}</h2>
                    <h4 class="o50">Study Type</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.study_type}</h2>
                    <h4 class="o50">Study Type</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.start_date}</h2>
                    <h4 class="o50">Start Date</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.completion_date}</h2>
                    <h4 class="o50">Completion Date</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.purpose}</h2>
                    <h4 class="o50">Trial Purpose</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.enrollment}</h2>
                    <h4 class="o50">Enrollment</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.gender}</h2>
                    <h4 class="o50">Gender</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.minimumAge}</h2>
                    <h4 class="o50">Minimum Age</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{trial.maximumAge}</h2>
                    <h4 class="o50">Maximum Age</h4>
                </div>
                <div class="num--vacc border">
                    <h4 class="o50">Description</h4>
                    <h5>{trial.description}</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 num--vacc border">
                    <h4 class="o50">
                        Participating Countries' Beliefs on COVID-19
                    </h4>
                    <p>
                        Click on a country to see its population's COVID-19
                        beliefs.
                    </p>
                    {displayBeliefLinks(beliefs)}
                </div>
                <div class="col-sm-6 num--vacc border">
                    <h4 class="o50">Participating Countries</h4>
                    <p>
                        Click on a country to see how it's doing at vaccinating
                        its citizens.
                    </p>
                    {displayCountryLinks(countries)}
                </div>
            </div>
            <br></br>
            <div>
                <h4 className="card-text">Top News Articles</h4>
                <p>
                    Click to read one of the top news articles relating to this
                    clinical trial.
                </p>
                {displayNewsArticles(trial.articles)}
            </div>
        </div>
    )
}
export default ClinicalTrialsInstance
