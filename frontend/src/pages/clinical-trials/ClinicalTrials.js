import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import MUIDataTable from 'mui-datatables'
import Highlighter from 'react-highlight-words'
export const clinical_trial_data = []

function ClinicalTrials() {
    var [trials, setTrials] = useState([])
    let history = useHistory()
    const [searchText, setSearchText] = useState('')
    const CustomBodyRender = (value, tableMeta, updateValue) => (
        <div>
            <Highlighter
                searchWords={[searchText]}
                textToHighlight={value + ''}
            ></Highlighter>
        </div>
    )
    // fetch country data and render on callback
    useEffect(() => {
        const url = 'https://vaxtracker.me/api/trials'
        fetchJSON(url).then((data) => {
            setTrials(data)
        })
    }, [])

    // generic async JSON fetching code
    async function fetchJSON(url) {
        var res = await fetch(url)
        return res.json()
    }
    const columns = [
        {
            label: 'ID',
            name: 'trial_id',
            options: {
                display: 'excluded',
                filter: false,
                sort: false,
            },
        },
        {
            label: 'Title',
            name: 'title',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Status',
            name: 'status',
            options: {
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Study Type',
            name: 'study_type',
            options: {
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Study Phase',
            name: 'study_phase',
            options: {
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Start Date',
            name: 'start_date',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Completion Date',
            name: 'completion_date',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Purpose',
            name: 'purpose',
            options: {
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Enrollment',
            name: 'enrollment',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Country',
            name: 'country',
            options: {
                filterType: 'checkbox',
                filterOptions: {
                    names: ['A-I', 'J-R', 'S-Z'],
                    logic(country_name, filterVal) {
                        const show =
                            (filterVal.indexOf('A-I') >= 0 &&
                                country_name.charCodeAt(0) >=
                                    'A'.charCodeAt(0) &&
                                country_name.charCodeAt(0) <=
                                    'I'.charCodeAt(0)) ||
                            (filterVal.indexOf('J-R') >= 0 &&
                                country_name.charCodeAt(0) >=
                                    'J'.charCodeAt(0) &&
                                country_name.charCodeAt(0) <=
                                    'R'.charCodeAt(0)) ||
                            (filterVal.indexOf('S-Z') >= 0 &&
                                country_name.charCodeAt(0) >=
                                    'S'.charCodeAt(0) &&
                                country_name.charCodeAt(0) <= 'Z'.charCodeAt(0))
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
    ]

    return (
        <div
            style={{
                minHeight: '900px',
                margin: 'auto',
                paddingTop: '70px',
                width: '80%',
            }}
        >
            <h2>Covid-19 Clinical Trials</h2>
            <h5>Click on a clinical trial to learn more about its status.</h5>
            <br></br>

            <MUIDataTable
                data={trials}
                columns={columns}
                options={{
                    filterType: 'checkbox',
                    onRowClick: (rowData) => {
                        history.push('/ClinicalTrials/' + rowData[0])
                    },
                    customSearch: (searchQuery, currentRow, columns) => {
                        var match = false
                        setSearchText(searchQuery)
                        currentRow.forEach((col) => {
                            if (
                                col
                                    .toString()
                                    .toLowerCase()
                                    .includes(searchQuery.toLowerCase())
                            ) {
                                match = true
                            }
                        })
                        if (searchQuery.length < 2) {
                            setSearchText('')
                        }
                        return match
                    },
                    onSearchClose: () => {
                        setSearchText('')
                    },
                }}
            />
        </div>
    )
}
export default ClinicalTrials
