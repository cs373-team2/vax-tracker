import React from 'react'
import VaccCard from './VaccCard'
import { Grid } from '@material-ui/core'

function VaccGrid({ data, searchQuery }) {
    return (
        <Grid container spacing={3} justify="center">
            {data.map((col) => {
                return (
                    <Grid item key={col.vaccine_id} xs={4}>
                        <VaccCard data={col} searchQuery={searchQuery} />
                    </Grid>
                )
            })}
        </Grid>
    )
}
export default VaccGrid
