import React from 'react'
import { useEffect, useState } from 'react'
import './Search.css'
import { Pagination } from '@material-ui/lab'
import CountryGrid from '../countries/CountryGrid'
import VaccGrid from './VaccGrid'
import BeliefGrid from './BeliefGrid'
import TrialGrid from './TrialGrid'
import { makeStyles } from '@material-ui/core/styles'

import GeneralSearchBar from './GeneralSearchBar.js'

const useStyles = makeStyles((theme) => ({
    pagination: {
        justifyContent: 'center',
        display: 'flex',
        '& > *': {
            marginTop: theme.spacing(2),
        },
    },
}))

export function thousands_separators(num) {
    if (num === undefined) return
    var num_parts = num.toString().split('.')
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    return num_parts.join('.')
}

function Search() {
    const CardsPerPage = 3
    const styles = useStyles()
    var [countries, setCountries] = useState([])
    var [trials, setTrials] = useState([])
    var [vaccs, setVaccs] = useState([])
    var [beliefs, setBeliefs] = useState([])

    var [currentCountry, setCurrentCountry] = useState([])
    var [currentVaccs, setCurrentVaccs] = useState([])
    var [currentTrials, setCurrentTrials] = useState([])
    var [currentBeliefs, setCurrentBeliefs] = useState([])

    var [searchCountry, setSearchCountry] = useState([])
    var [searchVacc, setSearchVacc] = useState([])
    var [searchBelief, setSearchBelief] = useState([])
    var [searchTrial, setSearchTrial] = useState([])

    var [searchQuery, setSeachQuery] = useState('')

    async function updateSearchQuery(searchQuery) {
        var filtered = []
        if (searchQuery !== '') {
            filtered = countries.filter((data) => {
                var cur = data.name
                    .toLowerCase()
                    .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    thousands_separators(data.population)
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.capital
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    thousands_separators(data.total_doses_admin)
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    thousands_separators(data.total_people_vacc)
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.total_vaccinations_per100
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.approved_vaccines
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                return cur
            })
        }

        setSeachQuery(searchQuery)
        setCurrentCountry(filtered.slice(0, CardsPerPage))
        setSearchCountry(filtered)

        //Beliefs
        filtered = []
        if (searchQuery !== '') {
            filtered = beliefs.filter((data) => {
                var cur = data.country_name
                    .toLowerCase()
                    .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.perc_would_take_vaccine
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.perc_side_effects
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.perc_dont_believe
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.mask_extremely
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.mask_very
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.mask_moderately
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.mask_slightly
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.mask_not
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                return cur
            })
        }

        setSearchBelief(filtered)
        setCurrentBeliefs(filtered.slice(0, CardsPerPage))

        //Vaccs
        filtered = []
        if (searchQuery !== '') {
            filtered = vaccs.filter((data) => {
                var cur = data.name
                    .toLowerCase()
                    .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.num_approved_countries
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.sponsors
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.mechanism
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.trial_phase
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.efficacy
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.dosage
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.method
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.storage
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.institutions
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                return cur
            })
        }

        setSearchVacc(filtered)
        setCurrentVaccs(filtered.slice(0, CardsPerPage))

        //Trials
        filtered = []
        if (searchQuery !== '') {
            filtered = trials.filter((data) => {
                var cur = data.title
                    .toLowerCase()
                    .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.start_date
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.completion_date
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.enrollment
                        .toString()
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.status
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.study_type
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.study_phase
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.purpose
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                cur =
                    cur ||
                    data.country
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase())
                return cur
            })
        }
        setSearchTrial(filtered)
        setCurrentTrials(filtered.slice(0, CardsPerPage))
    }
    // TODO: implement server-side pageination
    // fetch country data and render on callback
    useEffect(() => {
        const url = 'https://vaxtracker.me/api/countries'
        fetchJSON(url).then((data) => {
            setCountries(data)
        })
    }, [])
    useEffect(() => {
        const url = 'https://vaxtracker.me/api/vaccines'
        fetchJSON(url).then((data) => {
            setVaccs(data)
        })
    }, [])
    useEffect(() => {
        const url = 'https://vaxtracker.me/api/trials'
        fetchJSON(url).then((data) => {
            setTrials(data)
        })
    }, [])
    useEffect(() => {
        const url = 'https://vaxtracker.me/api/beliefs'
        fetchJSON(url).then((data) => {
            setBeliefs(data)
        })
    }, [])

    // generic async JSON fetching code
    async function fetchJSON(url) {
        var res = await fetch(url)
        return res.json()
    }

    // update current "data frame"
    function handleChangePage(pageNumber, model) {
        if (model === 'country') {
            setCurrentCountry(
                searchCountry.slice(
                    pageNumber * CardsPerPage - CardsPerPage,
                    pageNumber * CardsPerPage
                )
            )
        }
        if (model === 'vaccs') {
            setCurrentVaccs(
                searchVacc.slice(
                    pageNumber * CardsPerPage - CardsPerPage,
                    pageNumber * CardsPerPage
                )
            )
        }
        if (model === 'beliefs') {
            setCurrentBeliefs(
                searchBelief.slice(
                    pageNumber * CardsPerPage - CardsPerPage,
                    pageNumber * CardsPerPage
                )
            )
        }
        if (model === 'trials') {
            setCurrentTrials(
                searchTrial.slice(
                    pageNumber * CardsPerPage - CardsPerPage,
                    pageNumber * CardsPerPage
                )
            )
        }
    }

    return (
        <div className="container" style={{ paddingTop: '70px' }}>
            <h2>Search Any Models</h2>
            <h5>
                Search for a country, a vaccine, a belief, or a vaccine trial
                here.{' '}
            </h5>

            <GeneralSearchBar
                searchQuery={searchQuery}
                onChange={updateSearchQuery}
            />
            <br></br>
            <br></br>

            <h2>Countries</h2>
            <CountryGrid
                data={currentCountry}
                searchQuery={searchQuery}
            ></CountryGrid>
            <div className={styles.pagination}>
                <Pagination
                    count={Math.ceil(searchCountry.length / CardsPerPage)}
                    onChange={(_, page) => handleChangePage(page, 'country')}
                    showFirstButton
                    showLastButton
                />
                <h5>Total Matched: {searchCountry.length}</h5>
            </div>
            <br></br>

            <h2>Vaccines</h2>
            <VaccGrid data={currentVaccs} searchQuery={searchQuery}></VaccGrid>
            <div className={styles.pagination}>
                <Pagination
                    count={Math.ceil(searchVacc.length / CardsPerPage)}
                    onChange={(_, page) => handleChangePage(page, 'vaccs')}
                    showFirstButton
                    showLastButton
                />
                <h5>Total Matched: {searchVacc.length}</h5>
            </div>
            <br></br>

            <h2>Trials</h2>
            <TrialGrid
                data={currentTrials}
                searchQuery={searchQuery}
            ></TrialGrid>
            <div className={styles.pagination}>
                <Pagination
                    count={Math.ceil(searchTrial.length / CardsPerPage)}
                    onChange={(_, page) => handleChangePage(page, 'trials')}
                    showFirstButton
                    showLastButton
                />
                <h5>Total Matched: {searchTrial.length}</h5>
            </div>
            <br></br>

            <h2>Beliefs</h2>
            <BeliefGrid
                data={currentBeliefs}
                searchQuery={searchQuery}
            ></BeliefGrid>
            <div className={styles.pagination}>
                <Pagination
                    count={Math.ceil(searchBelief.length / CardsPerPage)}
                    onChange={(_, page) => handleChangePage(page, 'beliefs')}
                    showFirstButton
                    showLastButton
                />
                <h5>Total Matched: {searchBelief.length}</h5>
            </div>
        </div>
    )
}

export default Search
