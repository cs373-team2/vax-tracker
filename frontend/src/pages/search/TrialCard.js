import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
// import { CardMedia } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import Highlighter from 'react-highlight-words'

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        height: 170,
    },
})

export default function TrialCard({ data, searchQuery }) {
    const classes = useStyles()
    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Typography
                    className={classes.pos}
                    variant="h6"
                    component="h2"
                    align="center"
                >
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.title}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Status:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.status}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Study Type:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.study_type}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Study Phase:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.study_phase}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Start Date:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.start_date}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Completion Date:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.completion_date}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Purpose:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.purpose}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Enrollment:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.enrollment.toString()}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Countries:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.country}
                    />
                </Typography>
            </CardContent>
            <CardActions>
                <Button
                    id="learn_more"
                    size="small"
                    href={'/ClinicalTrials/' + data.trial_id}
                >
                    Learn More{' '}
                </Button>
            </CardActions>
        </Card>
    )
}
