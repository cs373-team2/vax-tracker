import React from 'react'
import BeliefCard from './BeliefCard'
import { Grid } from '@material-ui/core'

function BeliefGrid({ data, searchQuery }) {
    return (
        <Grid container spacing={3} justify="center">
            {data.map((col) => {
                return (
                    <Grid item key={col.country_id} xs={4}>
                        <BeliefCard data={col} searchQuery={searchQuery} />
                    </Grid>
                )
            })}
        </Grid>
    )
}
export default BeliefGrid
