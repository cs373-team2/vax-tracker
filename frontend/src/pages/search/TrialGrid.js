import React from 'react'
import TrialCard from './TrialCard'
import { Grid } from '@material-ui/core'

function TrialGrid({ data, searchQuery }) {
    return (
        <Grid container spacing={3} justify="center">
            {data.map((col) => {
                return (
                    <Grid item key={col.trial_id} xs={4}>
                        <TrialCard data={col} searchQuery={searchQuery} />
                    </Grid>
                )
            })}
        </Grid>
    )
}
export default TrialGrid
