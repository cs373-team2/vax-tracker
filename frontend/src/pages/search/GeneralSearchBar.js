import React from 'react'

const SearchBar = ({ searchQuery: keyword, onChange: setKeyword }) => {
    const BarStyling = {
        width: '30rem',
        background: '#F2F1F9',
        border: 'none',
        padding: '0.5rem',
    }
    return (
        <input
            style={BarStyling}
            key="random1"
            value={keyword}
            placeholder={'Enter sitewide search here'}
            onChange={(e) => setKeyword(e.target.value)}
        />
    )
}

export default SearchBar
