import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
// import { CardMedia } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import Highlighter from 'react-highlight-words'

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        height: 170,
    },
})

export default function BeliefCard({ data, searchQuery }) {
    const classes = useStyles()
    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Typography
                    className={classes.pos}
                    variant="h6"
                    component="h2"
                    align="center"
                >
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.country_name}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Would Take Vaccine:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.perc_would_take_vaccine.toString()}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Side Effect Concerns:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.perc_side_effects.toString()}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Don't Believe Vaccine Efficacy:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.perc_dont_believe.toString()}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Masks Extremely Effective:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.mask_extremely.toString()}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Masks Very Effective:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.mask_very.toString()}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Masks Moderately Effective:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.mask_moderately.toString()}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Masks Slightly Effective:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.mask_slightly.toString()}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Masks Not Effective:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.mask_not.toString()}
                    />
                </Typography>
            </CardContent>
            <CardActions>
                <Button
                    id="learn_more"
                    size="small"
                    href={'/Beliefs/' + data.id}
                >
                    Learn More{' '}
                </Button>
            </CardActions>
        </Card>
    )
}
