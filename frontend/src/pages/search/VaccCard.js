import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
// import { CardMedia } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import Highlighter from 'react-highlight-words'

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        height: 170,
    },
})

export default function VaccCard({ data, searchQuery }) {
    const classes = useStyles()
    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Typography
                    className={classes.pos}
                    variant="h6"
                    component="h2"
                    align="center"
                >
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.name}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Mechanism:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.mechanism}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Sponsors:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.sponsors}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Trial Phase:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.trial_phase}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Efficacy:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.efficacy}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Dosage:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.dosage}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Type:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.method}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Storage:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.storage}
                    />
                </Typography>

                <Typography className={classes.pos}>
                    Institutions:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.institutions}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Number of Approved Countries:
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={data.num_approved_countries.toString()}
                    />
                </Typography>
            </CardContent>
            <CardActions>
                <Button
                    id="learn_more"
                    size="small"
                    href={'/Vaccines/' + data.vaccine_id}
                >
                    Learn More{' '}
                </Button>
            </CardActions>
        </Card>
    )
}
