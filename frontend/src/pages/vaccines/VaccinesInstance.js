import React from 'react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Grid } from '@material-ui/core/'
import { makeStyles } from '@material-ui/core/styles'
import NewsCard from '../../components/cards/NewsCard'

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    cards: {
        flexGrow: 1,
        padding: theme.spacing(2),
    },
}))

const VaccinesInstance = ({ match }) => {
    const classes = useStyles()
    const [vaccine, setVaccine] = useState([])
    const [approvedCountries, setApprovedCountries] = useState([])

    const setRelationshipData = (data) => {
        let arr = []
        for (let i = 0; i < Object.keys(data).length; i++) {
            arr.push(data[i]['country_id'])
        }
        if (arr) {
            let id_list = arr.join()
            return id_list
        } else {
            return 'None'
        }
    }

    // load all vaccine data and associated countries
    useEffect(() => {
        async function fetchJSON(url) {
            var res = await fetch(url)
            return res.json()
        }
        // fetch vaccine data
        async function fetchVaccine(id) {
            let vaccineEntry = await fetchJSON(
                'https://vaxtracker.me/api/vaccines?vaccineId=' + id
            )
            setVaccine(vaccineEntry[0])
            let links = await fetchJSON(
                'https://vaxtracker.me/api/vaccine_link?vaccineId=' + id
            )
            let idList = setRelationshipData(links)
            let approved_countries = []
            if (idList.localeCompare('None')) {
                approved_countries = await fetchJSON(
                    'https://vaxtracker.me/api/countries?countryId=' + idList
                )
            }
            setApprovedCountries(approved_countries)
        }
        fetchVaccine(match.params.id)
    } ,[match.params.id])

    function displayNewsArticles(articles) {
        var present = false
        if (articles) {
            var articlesJSON = JSON.parse(articles)
            // console.log(articlesJSON)
            present = true
        }
        if (present) {
            return (
                <div className={classes.cards}>
                    <Grid
                        container
                        spacing={2}
                        direction="row"
                        justify="center"
                        alignItems="flex-start"
                        width="100%"
                    >
                        {articlesJSON.map((article) => (
                            <Grid item key={articlesJSON.indexOf(article)}>
                                <NewsCard article={article} />
                            </Grid>
                        ))}
                    </Grid>
                </div>
            )
        } else {
            return <div></div>
        }
    }

    function displayLogos(logos) {
        if (logos) {
            var arr = logos.split(', ')
            return (
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    {arr.map((logo) => (
                        <img
                            alt=""
                            src={logo}
                            style={{
                                width: '320px',
                                height: '140px',
                                marginLeft: '2rem',
                            }}
                        ></img>
                    ))}
                </div>
            )
        }
    }

    // console.log(approvedCountries)

    // if (!status) return <Spinner />
    return (
        <div class="container country--page2">
            <div class="row border">
                <div class="col-sm-4 num--vacc border">
                    <h2 class="country--name">{vaccine.name}</h2>
                </div>
                <div class="col-sm-8 num--vacc border">
                    {displayLogos(vaccine.logos)}
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.mechanism}</h2>
                    <h4 class="o50">Mechanism</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.sponsors}</h2>
                    <h4 class="o50">Sponsors</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.trial_phase}</h2>
                    <h4 class="o50">Trial Phase</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.institutions}</h2>
                    <h4 class="o50">Institutions</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.efficacy}</h2>
                    <h4 class="o50">Efficacy</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.dosage}</h2>
                    <h4 class="o50">Dosage</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.storage}</h2>
                    <h4 class="o50">Storage</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.method}</h2>
                    <h4 class="o50">Method</h4>
                </div>
                <div class="col-sm-4 num--vacc border">
                    <h2>{vaccine.num_approved_countries}</h2>
                    <h4 class="o50">Number of Approved Countries</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 sublabel border">
                    <h3>Approved Countries</h3>
                    <p>
                        Click on a country to learn more about its COVID-19
                        statistics.
                    </p>
                </div>
            </div>
            <div class="row border">
                <div class="col-sm-12 col-reset mtable">
                    <div class="card entry">
                        <ul class="list-group list-group-flush">
                            {approvedCountries.length > 0 &&
                                approvedCountries.map((country) => (
                                    <li className="list-group-item">
                                        <Link
                                            to={
                                                '/globalvacc/' +
                                                country.country_id
                                            }
                                        >
                                            {country.name}
                                        </Link>
                                    </li>
                                ))}
                            {approvedCountries.length === 0 && (
                                <li className="list-group-item">
                                    Nothing here yet
                                </li>
                            )}
                        </ul>
                    </div>
                </div>
            </div>
            <br></br>
            <div>
                <h4 className="card-text">Top News Articles</h4>
                <p>
                    Click to read one of the top news articles for this vaccine.
                </p>
                {displayNewsArticles(vaccine.articles)}
            </div>
        </div>
    )
}
export default VaccinesInstance
