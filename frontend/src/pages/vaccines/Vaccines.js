import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import MUIDataTable from 'mui-datatables'
import Highlighter from 'react-highlight-words'

function Vaccines() {
    var [vaccines, setVaccines] = useState([])
    let history = useHistory()

    const [searchText, setSearchText] = useState('')
    const CustomBodyRender = (value, tableMeta, updateValue) => (
        <div>
            <Highlighter
                searchWords={[searchText]}
                textToHighlight={value + ''}
            ></Highlighter>
        </div>
    )
    // fetch country data and render on callback
    useEffect(() => {
        const url = 'https://vaxtracker.me/api/vaccines'
        fetchJSON(url).then((data) => {
            setVaccines(data)
        })
    }, [])

    // generic async JSON fetching code
    async function fetchJSON(url) {
        var res = await fetch(url)
        return res.json()
    }

    const columns = [
        {
            label: 'ID',
            name: 'vaccine_id',
            options: {
                display: 'excluded',
                filter: false,
                sort: false,
            },
        },
        {
            label: 'Name',
            name: 'name',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Mechanism',
            name: 'mechanism',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Sponsors',
            name: 'sponsors',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Trial Phase',
            name: 'trial_phase',
            options: {
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Efficacy',
            name: 'efficacy',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Dosage',
            name: 'dosage',
            options: {
                filterOptions: {
                    names: ['1 dose', '2 doses', '3 doses', 'TBD'],
                    logic(doseage, filterVal) {
                        const show =
                            (filterVal.indexOf('1 dose') >= 0 &&
                                doseage === '1 dose') ||
                            (filterVal.indexOf('2 doses') >= 0 &&
                                doseage.includes('2 doses')) ||
                            (filterVal.indexOf('3 doses') >= 0 &&
                                doseage.includes('3 doses')) ||
                            (filterVal.indexOf('TBD') >= 0 && doseage === 'TBD')
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Type',
            name: 'method',
            options: {
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Storage',
            name: 'storage',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Institutions',
            name: 'institutions',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Number of Approved Countries',
            name: 'num_approved_countries',
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        // {
        //   label: "Approved Countries",
        //   name: "approved_countries"
        // }
    ]
    return (
        <div
            style={{
                minHeight: '900px',
                margin: 'auto',
                paddingTop: '70px',
                width: '80%',
            }}
        >
            <h2>Covid-19 Vaccinations</h2>
            <h5>
                Click on a vaccination to learn more about it and where it's
                been approved.
            </h5>
            <br></br>

            <MUIDataTable
                data={vaccines}
                columns={columns}
                options={{
                    onRowClick: (rowData) => {
                        history.push('/Vaccines/' + rowData[0])
                    },
                    filterType: 'checkbox',
                    customSearch: (searchQuery, currentRow, columns) => {
                        var match = false
                        setSearchText(searchQuery)
                        currentRow.forEach((col) => {
                            if (
                                col
                                    .toString()
                                    .toLowerCase()
                                    .includes(searchQuery.toLowerCase())
                            ) {
                                match = true
                            }
                        })
                        if (searchQuery.length < 2) {
                            setSearchText('')
                        }
                        return match
                    },
                    onSearchClose: () => {
                        setSearchText('')
                    },
                }}
            />
        </div>
    )
}
export default Vaccines
