import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import MUIDataTable from 'mui-datatables'
import Highlighter from 'react-highlight-words'

function Beliefs() {
    var [beliefs, setBeliefs] = useState([])

    const [searchText, setSearchText] = useState('')

    let history = useHistory()
    // fetch country data and render on callback
    useEffect(() => {
        const url = 'https://vaxtracker.me/api/beliefs'
        fetchJSON(url).then((data) => {
            setBeliefs(data)
        })
    }, [])

    // generic async JSON fetching code
    async function fetchJSON(url) {
        var res = await fetch(url)
        return res.json()
    }

    const CustomBodyRender = (value, tableMeta, updateValue) => (
        <div>
            <Highlighter
                searchWords={[searchText]}
                textToHighlight={value + ''}
            ></Highlighter>
        </div>
    )
    const columns = [
        {
            label: 'ID',
            name: 'id',
            options: {
                display: 'excluded',
                filter: false,
                sort: false,
            },
        },
        {
            label: 'Country',
            name: 'country_name',
            options: {
                filterType: 'checkbox',
                filterOptions: {
                    names: ['A-I', 'J-R', 'S-Z'],
                    logic(city_name, filterVal) {
                        const show =
                            (filterVal.indexOf('A-I') >= 0 &&
                                city_name.charCodeAt(0) >= 'A'.charCodeAt(0) &&
                                city_name.charCodeAt(0) <= 'I'.charCodeAt(0)) ||
                            (filterVal.indexOf('J-R') >= 0 &&
                                city_name.charCodeAt(0) >= 'J'.charCodeAt(0) &&
                                city_name.charCodeAt(0) <= 'R'.charCodeAt(0)) ||
                            (filterVal.indexOf('S-Z') >= 0 &&
                                city_name.charCodeAt(0) >= 'S'.charCodeAt(0) &&
                                city_name.charCodeAt(0) <= 'Z'.charCodeAt(0))
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Would Take Vaccine',
            name: 'perc_would_take_vaccine',
            options: {
                filterOptions: {
                    names: ['0.00-0.25', '0.26-0.50', '0.51-0.75', '0.76-1.00'],
                    logic(perc, filterVal) {
                        const show =
                            (filterVal.indexOf('0.00-0.25') >= 0 &&
                                perc <= 0.25) ||
                            (filterVal.indexOf('0.26-0.50') >= 0 &&
                                perc > 0.25 &&
                                perc <= 0.5) ||
                            (filterVal.indexOf('0.51-0.75') >= 0 &&
                                perc > 0.5 &&
                                perc <= 0.75) ||
                            (filterVal.indexOf('0.76-1.00') >= 0 &&
                                perc > 0.75 &&
                                perc <= 1.0)
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Side Effect Concerns',
            name: 'perc_side_effects',
            options: {
                filterOptions: {
                    names: ['0.00-0.25', '0.26-0.50', '0.51-0.75', '0.76-1.00'],
                    logic(perc, filterVal) {
                        const show =
                            (filterVal.indexOf('0.00-0.25') >= 0 &&
                                perc <= 0.25) ||
                            (filterVal.indexOf('0.26-0.50') >= 0 &&
                                perc > 0.25 &&
                                perc <= 0.5) ||
                            (filterVal.indexOf('0.51-0.75') >= 0 &&
                                perc > 0.5 &&
                                perc <= 0.75) ||
                            (filterVal.indexOf('0.76-1.00') >= 0 &&
                                perc > 0.75 &&
                                perc <= 1.0)
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: "Don't Believe Vaccine Efficacy",
            name: 'perc_dont_believe',
            options: {
                filterOptions: {
                    names: ['0.00-0.25', '0.26-0.50', '0.51-0.75', '0.76-1.00'],
                    logic(perc, filterVal) {
                        const show =
                            (filterVal.indexOf('0.00-0.25') >= 0 &&
                                perc <= 0.25) ||
                            (filterVal.indexOf('0.26-0.50') >= 0 &&
                                perc > 0.25 &&
                                perc <= 0.5) ||
                            (filterVal.indexOf('0.51-0.75') >= 0 &&
                                perc > 0.5 &&
                                perc <= 0.75) ||
                            (filterVal.indexOf('0.76-1.00') >= 0 &&
                                perc > 0.75 &&
                                perc <= 1.0)
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Masks Extremely Effective',
            name: 'mask_extremely',
            options: {
                filterOptions: {
                    names: ['0.00-0.25', '0.26-0.50', '0.51-0.75', '0.76-1.00'],
                    logic(perc, filterVal) {
                        const show =
                            (filterVal.indexOf('0.00-0.25') >= 0 &&
                                perc <= 0.25) ||
                            (filterVal.indexOf('0.26-0.50') >= 0 &&
                                perc > 0.25 &&
                                perc <= 0.5) ||
                            (filterVal.indexOf('0.51-0.75') >= 0 &&
                                perc > 0.5 &&
                                perc <= 0.75) ||
                            (filterVal.indexOf('0.76-1.00') >= 0 &&
                                perc > 0.75 &&
                                perc <= 1.0)
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Masks Very Effective',
            name: 'mask_very',
            options: {
                filterOptions: {
                    names: ['0.00-0.25', '0.26-0.50', '0.51-0.75', '0.76-1.00'],
                    logic(perc, filterVal) {
                        const show =
                            (filterVal.indexOf('0.00-0.25') >= 0 &&
                                perc <= 0.25) ||
                            (filterVal.indexOf('0.26-0.50') >= 0 &&
                                perc > 0.25 &&
                                perc <= 0.5) ||
                            (filterVal.indexOf('0.51-0.75') >= 0 &&
                                perc > 0.5 &&
                                perc <= 0.75) ||
                            (filterVal.indexOf('0.76-1.00') >= 0 &&
                                perc > 0.75 &&
                                perc <= 1.0)
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Masks Moderately Effective',
            name: 'mask_moderately',
            options: {
                filterOptions: {
                    names: ['0.00-0.25', '0.26-0.50', '0.51-0.75', '0.76-1.00'],
                    logic(perc, filterVal) {
                        const show =
                            (filterVal.indexOf('0.00-0.25') >= 0 &&
                                perc <= 0.25) ||
                            (filterVal.indexOf('0.26-0.50') >= 0 &&
                                perc > 0.25 &&
                                perc <= 0.5) ||
                            (filterVal.indexOf('0.51-0.75') >= 0 &&
                                perc > 0.5 &&
                                perc <= 0.75) ||
                            (filterVal.indexOf('0.76-1.00') >= 0 &&
                                perc > 0.75 &&
                                perc <= 1.0)
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Masks Slightly Effective',
            name: 'mask_slightly',
            options: {
                filterOptions: {
                    names: ['0.00-0.25', '0.26-0.50', '0.51-0.75', '0.76-1.00'],
                    logic(perc, filterVal) {
                        const show =
                            (filterVal.indexOf('0.00-0.25') >= 0 &&
                                perc <= 0.25) ||
                            (filterVal.indexOf('0.26-0.50') >= 0 &&
                                perc > 0.25 &&
                                perc <= 0.5) ||
                            (filterVal.indexOf('0.51-0.75') >= 0 &&
                                perc > 0.5 &&
                                perc <= 0.75) ||
                            (filterVal.indexOf('0.76-1.00') >= 0 &&
                                perc > 0.75 &&
                                perc <= 1.0)
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            label: 'Masks Not Effective',
            name: 'mask_not',
            options: {
                filterOptions: {
                    names: ['0.00-0.25', '0.26-0.50', '0.51-0.75', '0.76-1.00'],
                    logic(perc, filterVal) {
                        const show =
                            (filterVal.indexOf('0.00-0.25') >= 0 &&
                                perc <= 0.25) ||
                            (filterVal.indexOf('0.26-0.50') >= 0 &&
                                perc > 0.25 &&
                                perc <= 0.5) ||
                            (filterVal.indexOf('0.51-0.75') >= 0 &&
                                perc > 0.5 &&
                                perc <= 0.75) ||
                            (filterVal.indexOf('0.76-1.00') >= 0 &&
                                perc > 0.75 &&
                                perc <= 1.0)
                        return !show
                    },
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    CustomBodyRender(value, tableMeta, updateValue),
            },
        },
    ]

    return (
        // <div>
        <div
            style={{
                minHeight: '900px',
                margin: 'auto',
                paddingTop: '70px',
                width: '80%',
            }}
        >
            <h2>Beliefs On COVID-19/Vaccines Around the World</h2>
            <h5>
                Click on a country to learn more about the beliefs they hold
                about Covid-19.
            </h5>
            <br></br>

            <MUIDataTable
                data={beliefs}
                columns={columns}
                options={{
                    filterType: 'checkbox',
                    onRowClick: (rowData) => {
                        history.push('/Beliefs/' + rowData[0])
                    },
                    customSearch: (searchQuery, currentRow, columns) => {
                        var match = false

                        setSearchText(searchQuery)
                        currentRow.forEach((col) => {
                            if (
                                col
                                    .toString()
                                    .toLowerCase()
                                    .includes(searchQuery.toLowerCase())
                            ) {
                                match = true
                            }
                        })
                        if (searchQuery.length < 2) {
                            setSearchText('')
                        }
                        return match
                    },
                    onSearchClose: () => {
                        setSearchText('')
                    },
                }}
            />
        </div>
    )
}
export default Beliefs
