import React from 'react'
import { useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import PublicIcon from '@material-ui/icons/Public'
import { Bar } from 'react-chartjs-2'
import './BeliefsInstance.css'

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
}))

const BeliefsInstance = ({ match }) => {
    const classes = useStyles()
    const [belief, setBelief] = useState([])
    const [trials, setTrials] = useState([])
    const setRelationshipData = (data, field) => {
        let arr = []
        for (let i = 0; i < Object.keys(data).length; i++) {
            arr.push(data[i][field])
        }
        let id_list = arr.join()
        return id_list
    }

    useEffect(() => {
        // generic async JSON fetching code
        async function fetchJSON(url) {
            var res = await fetch(url)
            return res.json()
        }
        async function fetchBelief(id) {
            let beliefEntry = await fetchJSON(
                'https://vaxtracker.me/api/beliefs?beliefId=' + id
            )
            setBelief(beliefEntry[0])
            // console.log(belief.country_info_id)
            let links = await fetchJSON(
                'https://vaxtracker.me/api/trial_link?countryId=' +
                    beliefEntry[0]['country_info_id']
            )
            let idList = setRelationshipData(links, 'trial_id')
            // console.log(idList)
            let trials = await fetchJSON(
                'https://vaxtracker.me/api/trials?trialId=' + idList
            )
            setTrials(trials)
            // console.log(trials)
        }
        fetchBelief(match.params.id)
    }, [match.params.id])

    const state = {
        labels: [
            'Not Effective',
            'Slightly Effective',
            'Moderately Effective',
            'Very Effective',
            'Extremely Effective',
        ],
        datasets: [
            {
                label: 'Belief in Masks',
                backgroundColor: '#ECE3F0',
                borderColor: 'rgba(0,0,0,1)',
                borderWidth: 2,
                fontSize: 20,
                data: [
                    (belief.mask_not * 100).toFixed(2),
                    (belief.mask_slightly * 100).toFixed(2),
                    (belief.mask_moderately * 100).toFixed(2),
                    (belief.mask_very * 100).toFixed(2),
                    (belief.mask_extremely * 100).toFixed(2),
                ],
            },
        ],
    }
    return (
        <div style={{ paddingBottom: '0px' }}>
            <div class="container country--page2">
                <div class="row border">
                    <div class="col-sm-12 map">
                        <h2 class="country--name">{belief.country_name}</h2>
                    </div>
                </div>
                <div class="row country--visual border">
                    <div class="col-sm-12 map">
                        <img alt="" src={belief.flag} />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 num--vacc border">
                        <h2>
                            {(belief.perc_would_take_vaccine * 100).toFixed(2)}%
                        </h2>
                        <h4 class="o50">
                            Percent of Respondents Who Would Take Vaccine
                        </h4>
                    </div>
                    <div class="col-sm-4 num--vacc border">
                        <h2>{(belief.perc_side_effects * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Concerned About Side Effects
                        </h4>
                    </div>
                    <div class="col-sm-4 num--vacc border">
                        <h2>{(belief.perc_dont_believe * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Don't Believe Vaccine
                            Will Work
                        </h4>
                    </div>
                    <div class="col-sm-4 num--vacc border">
                        <h2>{(belief.accept_vax_yes * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Trust Future Vaccines
                        </h4>
                    </div>
                    <div class="col-sm-4 num--vacc border">
                        <h2>{(belief.accept_vax_no * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Do Not Trust Future
                            Vaccines
                        </h4>
                    </div>
                    <div class="col-sm-4 num--vacc border">
                        <h2>{(belief.accept_vax_idk * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Are Not Sure About The
                            Functionality of Future Vaccines
                        </h4>
                    </div>

                    <div class="col-sm-4 num--vacc border">
                        <h2>{(belief.mask_not * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Think Masks Are Not
                            Effective At All in Preventing COVID-19
                        </h4>
                    </div>
                    <div class="col-sm-4 num--vacc border">
                        <h2>{(belief.mask_slightly * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Think Masks Are Slightly
                            Effective in Preventing COVID-19
                        </h4>
                    </div>
                    <div class="col-sm-4 num--vacc border">
                        <h2>{(belief.mask_moderately * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Think Masks Are
                            Moderately Effective in Preventing COVID-19
                        </h4>
                    </div>
                    <div class="col-sm-6 num--vacc border">
                        <h2>{(belief.mask_very * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Think Masks Are Very
                            Effective in Preventing COVID-19
                        </h4>
                    </div>
                    <div class="col-sm-6 num--vacc border">
                        <h2>{(belief.mask_extremely * 100).toFixed(2)}%</h2>
                        <h4 class="o50">
                            Percent of Respondents Who Think Masks Are Extremely
                            Effective in Preventing COVID-19
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 sublabel border">
                        <h3>Clinical Trials</h3>
                        <p>Click on a trial to learn more about it.</p>
                    </div>
                    <div class="col-sm-6 sublabel border">
                        <h3>Country COVID-19 Vaccine Information</h3>
                        <p>
                            Click below to learn how this country is doing at
                            vaccinating its citizens.
                        </p>
                    </div>
                </div>
                <div class="row border">
                    <div class="col-sm-6 col-reset mtable">
                        <div class="card entry">
                            <ul class="list-group list-group-flush">
                                {trials.length > 0 &&
                                    trials.map((trial) => (
                                        <li className="list-group-item">
                                            <Link
                                                to={
                                                    '/ClinicalTrials/' +
                                                    trial.trial_id
                                                }
                                            >
                                                {trial.title}
                                            </Link>
                                        </li>
                                    ))}
                                {trials.length === 0 && (
                                    <li className="list-group-item">
                                        Nothing here yet
                                    </li>
                                )}
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 map">
                        <Button
                            id="vaccine_info"
                            variant="contained"
                            className={classes.button}
                            startIcon={<PublicIcon />}
                            href={'/globalvacc/' + belief.country_info_id}
                        >
                            {belief.country_name} COVID-19 Vaccine Info
                        </Button>
                    </div>
                </div>
            </div>
            <div>
                <Bar
                    className="bar_graph"
                    data={state}
                    options={{
                        title: {
                            display: true,
                            text: 'Belief in Masks to Prevent COVID-19',
                            fontSize: 27,
                        },
                        scales: {
                            xAxes: [
                                {
                                    ticks: {
                                        fontSize: 20,
                                    },
                                    scaleLabel: {
                                        display: true,
                                        fontSize: 23,
                                        labelString:
                                            'Levels of Belief in Mask Effectiveness',
                                    },
                                },
                            ],
                            yAxes: [
                                {
                                    ticks: {
                                        fontSize: 20,
                                    },
                                    scaleLabel: {
                                        display: true,
                                        fontSize: 23,
                                        labelString: 'Percent of Population',
                                    },
                                },
                            ],
                        },
                        legend: {
                            display: false,
                        },
                        layout: {
                            padding: {
                                left: 300,
                                right: 300,
                                top: 0,
                                bottom: 100,
                            },
                        },
                    }}
                />
            </div>
        </div>
    )
}
export default BeliefsInstance
