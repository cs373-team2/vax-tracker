import { makeStyles } from '@material-ui/core/styles'
import { IconButton, Collapse } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { useEffect, useState } from 'react'
import { Link as Scroll } from 'react-scroll'

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        height: '100vh',
        fontFamily: 'Nunito',
    },
    appbar: {
        background: 'none',
    },
    appbarWrapper: {
        width: '80%',
        margin: '0 auto',
    },
    colorText: {
        color: '#93329e',
    },
    title: {
        color: '#fff',
        fontSize: '3rem',
    },
    goDown: {
        color: '#93329e',
        fontSize: '2rem',
    },
}))

export default function Header() {
    const classes = useStyles()
    const [checked, setChecked] = useState(false)
    useEffect(() => {
        setChecked(true)
    }, [])
    return (
        <div className={classes.root} id="header">
            <Collapse
                in={checked}
                {...(checked ? { timeout: 1000 } : {})}
                collapsedHeight={10}
            >
                <div>
                    <h1 className={classes.title}>
                        Welcome to <br />
                        Vax<span className={classes.colorText}>Tracker.</span>
                    </h1>
                    <Scroll to="show-models" smooth={true}>
                        <IconButton>
                            <ExpandMoreIcon className={classes.goDown} />
                        </IconButton>
                    </Scroll>
                </div>
            </Collapse>
        </div>
    )
}
