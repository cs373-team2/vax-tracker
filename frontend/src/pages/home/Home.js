import React from 'react'
import './Home.css'
import { makeStyles } from '@material-ui/core/styles'
import { CssBaseline } from '@material-ui/core'
import Header from './Header'
import ShowModels from './ShowModels'

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
        backgroundImage: `url(${process.env.PUBLIC_URL + '/splash1.jpeg'})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        padding: { top: 0 },
    },
    content: {
        height: '100%',
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0.03)',
    },
}))

const Home = () => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <div className={classes.content}>
                <CssBaseline />
                <Header />
                <ShowModels />
            </div>
        </div>
    )
}
export default Home
