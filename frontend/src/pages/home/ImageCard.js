import React from 'react'
import { useState } from 'react'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import { Collapse } from '@material-ui/core'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        background: 'rgba(0,0,0,0.5)',
        margin: '20px',
    },
    media: {
        height: 240,
    },
    title: {
        fontFamily: 'Nunito',
        fontWeight: 'bold',
        fontSize: '2rem',
        color: '#fff',
    },
    desc: {
        fontFamily: 'Roboto',
        fontSize: '1.1rem',
        color: '#ddd',
    },
})

export default function ImageCard({ model, checked }) {
    const classes = useStyles()
    const [shadow, setShadow] = useState(1)

    const onMouseOver = () => setShadow(10)
    const onMouseOut = () => setShadow(1)

    return (
        <Link to={model.href}>
            <Collapse in={checked} {...(checked ? { timeout: 1000 } : {})}>
                <Card
                    onMouseEnter={onMouseOver}
                    onMouseLeave={onMouseOut}
                    elevation={shadow}
                    className={classes.root}
                >
                    <CardMedia
                        className={classes.media}
                        image={model.imageUrl}
                        title="model image"
                    />
                    <CardContent>
                        <Typography
                            gutterBottom
                            variant="h5"
                            component="h1"
                            className={classes.title}
                        >
                            {model.title}
                        </Typography>
                        <Typography
                            variant="body2"
                            color="textSecondary"
                            component="p"
                            className={classes.desc}
                        >
                            {model.desc}
                        </Typography>
                    </CardContent>
                </Card>
            </Collapse>
        </Link>
    )
}
