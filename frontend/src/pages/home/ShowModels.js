import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import ImageCard from './ImageCard'
import models from '../../static/models'
import useWindowPosition from '../../hooks/useWindowPosition'

const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: '100vh',
        paddingTop: '100px',
    },
    cards: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    container: {
        fontFamily: 'Nunito',
    },
}))

export default function ShowModels() {
    const classes = useStyles()
    const checked = useWindowPosition('header')
    return (
        <div className={classes.root}>
            <h2 className={classes.container}>Models</h2>
            <h4 className={classes.container}>
                Click on one of our models to learn more about it!
            </h4>
            <div className={classes.cards} id="show-models">
                <ImageCard model={models[0]} checked={checked} />
                <ImageCard model={models[1]} checked={checked} />
                <ImageCard model={models[2]} checked={checked} />
                <ImageCard model={models[3]} checked={checked} />
            </div>
        </div>
    )
}
