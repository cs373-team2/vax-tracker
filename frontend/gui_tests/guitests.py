import selenium
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

d_url = "http://localhost:3000"  # development
p_url = "https://www.vaxtracker.me"  # production


class vaxTrackerGUITests(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get(p_url)

    def test_countryinstance(self):
        # Test name: country_instance
        # Step # | name | target | value
        # 1 | open | / |
        # self.driver.get("https://vaxtracker.me/")
        # 2 | setWindowSize | 986x608 |
        self.driver.set_window_size(986, 608)
        # 3 | click | linkText=Global Data |
        self.driver.find_element(By.LINK_TEXT, "Global Data").click()
        self.driver.implicitly_wait(5)
        # 4 | click | css=.MuiGrid-root:nth-child(4) .MuiTypography-root:nth-child(4) |
        self.driver.find_element(
            By.CSS_SELECTOR,
            ".MuiGrid-root:nth-child(4) .MuiTypography-root:nth-child(4)",
        ).click()
        # 5 | close |  |
        self.driver.close()

    def test_beliefinstance(self):
        # Test name: belief_instance
        # Step # | name | target | value
        # 1 | open | / |
        # self.driver.get("https://vaxtracker.me/")
        # 2 | setWindowSize | 986x608 |
        self.driver.set_window_size(986, 608)
        # 3 | click | linkText=COVID Belief Data |
        self.driver.find_element(By.LINK_TEXT, "COVID Belief Data").click()
        self.driver.implicitly_wait(5)
        # 4 | click | css=#MUIDataTableBodyRow-4 > .MuiTableCell-root:nth-child(3) span > span |
        self.driver.find_element(
            By.CSS_SELECTOR,
            "#MUIDataTableBodyRow-4 > .MuiTableCell-root:nth-child(3) span > span",
        ).click()
        # 5 | runScript | window.scrollTo(0,248) |
        self.driver.execute_script("window.scrollTo(0,248)")
        # 6 | close |  |
        self.driver.close()

    def test_beliefinstancelink(self):
        # Test name: belief_instance_link
        # Step # | name | target | value
        # 1 | open | / |
        self.driver.get("https://vaxtracker.me/")
        # 2 | setWindowSize | 1936x1056 |
        self.driver.set_window_size(1936, 1056)
        # 3 | click | linkText=COVID Belief Data |
        self.driver.find_element(By.LINK_TEXT, "COVID Belief Data").click()
        self.driver.implicitly_wait(5)
        # 4 | click | css=#MUIDataTableBodyRow-6 > .MuiTableCell-root:nth-child(2) > .jss71 > div |
        self.driver.find_element(
            By.CSS_SELECTOR,
            "#MUIDataTableBodyRow-6 > .MuiTableCell-root:nth-child(2) > .jss71 > div",
        ).click()
        # 5 | runScript | window.scrollTo(0,0) |
        self.driver.implicitly_wait(5)
        self.driver.execute_script("window.scrollTo(0,0)")
        # 6 | click | linkText=A Phase 1, First-In-Human Study, to Assess the Safety, Reactogenicity, and Immunogenicity of the Investigational COVID-19 Vaccine SC-Ad6-1 in Healthy Volunteers |
        self.driver.find_element(
            By.LINK_TEXT,
            "A Phase 1, First-In-Human Study, to Assess the Safety, Reactogenicity, and Immunogenicity of the Investigational COVID-19 Vaccine SC-Ad6-1 in Healthy Volunteers",
        ).click()
        # 7 | runScript | window.scrollTo(0,419) |
        self.driver.execute_script("window.scrollTo(0,419)")
        # 8 | close |  |
        self.driver.close()

    def test_countryinstancelink(self):
        # Test name: country_instance_link
        # Step # | name | target | value
        # 1 | open | / |
        # self.driver.get("https://vaxtracker.me/")
        # 2 | setWindowSize | 986x608 |
        self.driver.set_window_size(986, 608)
        # 3 | click | linkText=Global Data |
        self.driver.find_element(By.LINK_TEXT, "Global Data").click()
        self.driver.implicitly_wait(5)
        # 4 | click | css=.MuiGrid-root:nth-child(4) .MuiCardMedia-root |
        self.driver.find_element(
            By.CSS_SELECTOR, ".MuiGrid-root:nth-child(4) .MuiCardMedia-root"
        ).click()
        # 5 | click | linkText=AZD1222 |
        self.driver.find_element(By.LINK_TEXT, "AZD1222").click()
        # 6 | close |  |
        self.driver.close()

    def test_trialsinstance(self):
        # Test name: trials_instance
        # Step # | name | target | value
        # 1 | open | / |
        # self.driver.get("https://vaxtracker.me/")
        # 2 | setWindowSize | 986x608 |
        self.driver.set_window_size(986, 608)
        # 3 | click | linkText=Clinical Trials |
        self.driver.find_element(By.LINK_TEXT, "Clinical Trials").click()
        self.driver.implicitly_wait(5)
        # 4 | click | css=#MUIDataTableBodyRow-2 > .MuiTableCell-root:nth-child(2) > .jss71 > div |
        self.driver.find_element(
            By.CSS_SELECTOR,
            "#MUIDataTableBodyRow-2 > .MuiTableCell-root:nth-child(2) > .jss71 > div",
        ).click()
        # 5 | runScript | window.scrollTo(0,200) |
        self.driver.execute_script("window.scrollTo(0,200)")
        # 6 | close |  |
        self.driver.close()

    def test_trialsinstancelink(self):
        # Test name: trials_instance_link
        # Step # | name | target | value
        # 1 | open | / |
        # self.driver.get("https://vaxtracker.me/")
        # 2 | setWindowSize | 986x608 |
        self.driver.set_window_size(986, 608)
        # 3 | click | linkText=Clinical Trials |
        self.driver.find_element(By.LINK_TEXT, "Clinical Trials").click()
        self.driver.implicitly_wait(5)
        # 4 | click | css=#MUIDataTableBodyRow-4 > .MuiTableCell-root:nth-child(2) > .jss71 > div |
        self.driver.find_element(
            By.CSS_SELECTOR,
            "#MUIDataTableBodyRow-4 > .MuiTableCell-root:nth-child(2) > .jss71 > div",
        ).click()
        self.driver.implicitly_wait(5)
        # 5 | runScript | window.scrollTo(0,543) |
        self.driver.execute_script("window.scrollTo(0,543)")
        # 6 | click | css=div:nth-child(3) > div > a |
        self.driver.find_element(By.CSS_SELECTOR, "div:nth-child(3) > div > a").click()
        # 7 | runScript | window.scrollTo(0,559) |
        self.driver.execute_script("window.scrollTo(0,559)")
        # 8 | close |  |
        self.driver.close()

    def test_vaccineinstance(self):
        # Test name: vaccine_instance
        # Step # | name | target | value
        # 1 | open | / |
        # self.driver.get("https://vaxtracker.me/")
        # 2 | setWindowSize | 986x608 |
        self.driver.set_window_size(986, 608)
        # 3 | click | linkText=COVID Vaccine Info |
        self.driver.find_element(By.LINK_TEXT, "COVID Vaccine Info").click()
        self.driver.implicitly_wait(5)
        # 4 | click | css=#MUIDataTableBodyRow-4 > .MuiTableCell-root:nth-child(3) > .jss71 > div |
        self.driver.find_element(
            By.CSS_SELECTOR,
            "#MUIDataTableBodyRow-4 > .MuiTableCell-root:nth-child(3) > .jss71 > div",
        ).click()
        # 5 | runScript | window.scrollTo(0,500) |
        self.driver.execute_script("window.scrollTo(0,500)")
        # 6 | close |  |
        self.driver.close()

    def test_vaccineinstancelink(self):
        # Test name: vaccine_instance_link
        # Step # | name | target | value
        # 1 | open | / |
        # self.driver.get("https://vaxtracker.me/")
        # 2 | setWindowSize | 986x608 |
        self.driver.set_window_size(986, 608)
        # 3 | click | linkText=COVID Vaccine Info |
        self.driver.find_element(By.LINK_TEXT, "COVID Vaccine Info").click()
        self.driver.implicitly_wait(5)
        # 4 | click | id=basic-navbar-nav |
        self.driver.find_element(By.ID, "basic-navbar-nav").click()
        # 5 | click | css=#MUIDataTableBodyRow-0 > .MuiTableCell-root:nth-child(3) |
        self.driver.find_element(
            By.CSS_SELECTOR, "#MUIDataTableBodyRow-0 > .MuiTableCell-root:nth-child(3)"
        ).click()
        # 6 | runScript | window.scrollTo(0,100) |
        self.driver.execute_script("window.scrollTo(0,100)")
        # 7 | click | linkText=Costa Rica |
        self.driver.find_element(By.LINK_TEXT, "Costa Rica").click()
        # 8 | runScript | window.scrollTo(0,0) |
        self.driver.execute_script("window.scrollTo(0,0)")
        # 9 | close |  |
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
