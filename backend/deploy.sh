echo "Deploying Backend..."
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin http://346446717640.dkr.ecr.us-east-1.amazonaws.com/
docker build -t vaxtracker-backend src
docker tag vaxtracker-backend:latest 346446717640.dkr.ecr.us-east-1.amazonaws.com/vaxtracker:latest
docker push 346446717640.dkr.ecr.us-east-1.amazonaws.com/vaxtracker:latest
cd aws_deploy
eb deploy
