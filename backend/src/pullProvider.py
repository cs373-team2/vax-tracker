import requests
import json
# cities_arr = []

# for i in range(40):
#     cities = requests.get(f'https://www.travvyexplorers.com/api/cities?page={i+1}').text
#     cities_arr.extend(json.loads(cities)['cities'])
# result_array = []
# for city in cities_arr:
#     result_dict = {}
#     result_dict['city_name'] = city['name']
#     result_dict['num_hotels'] = city['num_hotels']
#     result_dict['num_attractions'] = city['num_attractions']
#     result_dict['country'] = city['country']
#     result_dict['avg_hotel_rating'] = city['avg_hotel_rating']
#     result_array.append(result_dict)
# with open('cities.json', 'w') as fp:
#     json.dump(result_array, indent=2, ensure_ascii=False, fp=fp)

with open('cities.json', 'r') as fp:
    cities = json.loads(fp.read())

country_dict = {}
for city in cities:
    if city['avg_hotel_rating'] == None:
        continue
    if city['country'] not in country_dict:
        country_dict[city['country']] = city['num_hotels'] * [city['avg_hotel_rating']]
    else:
        country_dict[city['country']] + (city['num_hotels'] * [city['avg_hotel_rating']])

avg_dict = {}
for country in country_dict:
    avg_dict[country] = sum(country_dict[country]) / len(country_dict[country])

country_arr = []
for country in avg_dict:
    country_dict = {
        'country': country,
        'avg_hotel_rating': avg_dict[country]
    }
    country_arr.append(country_dict)

with open('hotel_ratings.json', 'w') as fp:
    json.dump(country_arr, indent=2, ensure_ascii=False, fp=fp)

# result_array = []
# attraction_types = ['interesting_places', 'architecture', 'cultural', 'historic', 'industrial_facilities', 'natural', 'religion', 'sport', 'shops', 'food', 'amusements']

# for attraction_type in attraction_types:
#     attractions = requests.get(f'https://www.travvyexplorers.com/api/attractions?AttractionCategory={attraction_type}').text
#     attractions_dict = json.loads(attractions)
#     result_dict = {}
#     result_dict['category'] = attraction_type
#     result_dict['num_attractions'] = attractions_dict['totalCount']
#     result_array.append(result_dict)

# with open('attractions.json', 'w') as fp:
#     json.dump(result_array, indent=2, ensure_ascii=False, fp=fp)
