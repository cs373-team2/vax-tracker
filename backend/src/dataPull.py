import pandas as pd
from sodapy import Socrata
import requests
import json
# Unauthenticated client only works with public data sets. Note 'None'
# in place of application token, and no username or password:
client = Socrata("data.cdc.gov", None)

# Example authenticated client (needed for non-public datasets):
# client = Socrata(data.cdc.gov,
#                  MyAppToken,
#                  userame="user@example.com",
#                  password="AFakePassword")

# First 2000 results, returned as JSON from API / converted to Python list of
# dictionaries by sodapy.
pfizer = client.get("saz5-9hgg", limit=2000)
moderna = client.get("b7pe-5nws", limit=2000)
# Convert to pandas DataFrame
pfizer_df = pd.DataFrame.from_records(pfizer)
moderna_df = pd.DataFrame.from_records(moderna)
# print(pfizer_df.head)
# print(pfizer_df.loc[pfizer_df['jurisdiction'] == 'California'])
# print(results2_df.head)

# request data from api
beliefs = requests.get("http://covidsurvey.mit.edu:5000/query?country=PO&signal={vaccine_accept,effect_mask}")
#convert json data to dic data for use!
beliefsData = json.loads(beliefs.text)

# beliefs_df = pd.DataFrame.from_records(beliefsData)
# print(beliefsData)


global_vaccines = requests.get("https://disease.sh/v3/covid-19/vaccine").text
global_vacc_data = json.loads(global_vaccines)
# print(global_vacc_data)


# request data from api
beliefs1 = requests.get("https://covidmap.umd.edu/api/resources?indicator={vaccine_acpt,barrier_reason_wontwork,hesitant_sideeffects}&type=smoothed&country=all&date=20210309").text
#convert json data to dic data for use!
beliefs1Data = json.loads(beliefs1)
# # beliefs1_df = pd.DataFrame.from_records(beliefs1Data)
# print(beliefs1Data)

trials = requests.get("https://ClinicalTrials.gov/api/query/study_fields?expr=covid19+vaccine&fields=BriefTitle,StudyType,Gender,MinimumAge,MaximumAge,BriefSummary,LocationFacility,LocationCity,LocationState,LocationZip,LocationCountry&min_rnk=1&max_rnk=5&fmt=json").text
trials_data = json.loads(trials)
print(trials_data)

# url = "https://countriesnow.space/api/v0.1/countries/flag/images"
# payload = "{\n\t\"country\": \"singapore\"\n}" 
# headers = {"Content-Type": "application/json"}
# response = requests.request("POST", url, headers=headers, data=payload).text
# data = json.loads(response)['data']['flag']
# print(data)

# url = "https://api.covid19api.com/live/country/singapore/status/confirmed/date/2020-03-24"
# payload={}
# headers = {"Content-Type": "application/json"}
# response = requests.request("GET", url, headers=headers, data=payload).text
# data = json.loads(response)
# length = len(data)
# print(data[length-1]['Active'])
