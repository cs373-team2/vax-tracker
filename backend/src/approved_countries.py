approved_countries = {
    "BNT162": "Albania, Andorra, Argentina, Aruba, Australia, Bahrain, Brazil, Canada, Caribbean, Chile, Colombia, Costa Rica, Ecuador, Austria, Belgium, Bulgaria, Croatia, Cyprus, Czechia, Denmark, Estonia, Finaland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden, Faroe Islands, Greenland, Hong Kong, Iceland, Iraq, Israel, Japan, Jordan, Kuwait, Lebanon, Liechtenstein, Malaysia, Mexico, Monaco, New Zealand, North Macedonia, Norway, Oman, Panama, Philippines, Qatar, Rwanda, Saint Vincent and the Grenadines, Saudi Arabia, Serbia, Singapore, South Korea, Suriname, Switzerland, United Arab Emirates, United Kingdom, United States, Vatican City, WHO",
    "mRNA-1273": "Andorra, Canada, Austria, Belgium, Bulgaria, Croatia, Cyprus, Czechia, Denmark, Estonia, Finaland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden, Faroe Islands, Greenland, Iceland, Israel, Liechtenstein, Norway, Qatar, Saint Vincent and the Grenadines, Singapore, Switzerland, United Kingdom, United States, Vietnam",
    "AZD1222": "Andorra, Argentina, Bahrain, Bangladesh, Barbados, Brazil, Canada, Chile, Dominican Republic, Ecuador, El Salvador, Egypt, Ethiopia, Austria, Belgium, Bulgaria, Croatia, Cyprus, Czechia, Denmark, Estonia, Finaland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden, Faroe Islands, Gambia, Ghana, Greenland, Guyana, Hungary, Iceland, India, Indonesia, Iraq, Ivory Coast, Malawi, Malaysia, Maldives, Mauritius, Mexico, Morocco, Myanmar, Nepal, Nigeria, Norway, Pakistan, Philippines, Rwanda, Saint Vincent and the Grenadines, Serbia, Sierra Leone, South Africa, South Korea, Sri Lanka, Sudan, Suriname, Taiwan, Thailand, United Kingdom, Vietnam",
    "Sputnik V": "Algeria, Angola, Argentina, Armenia, Azerbaijan, Bahrain, Belarus, Bolivia, Congo, Djibouti, Egypt, Gabon, Ghana, Guatemala, Guinea, Guyana, Honduras, Hungary, Iran, Iraq, Jordan, Kazakhstan, Kenya, Kyrgyzstan, Laos, Lebanon, Mexico, Moldova, Mongolia, Montenegro, Morocco, Myanmar, Namibia, Nicaragua, North Macedonia, Pakistan, Palestine, Paraguay, Republika Srpska, Russia, Saint Vincent and the Grenadines, San Marino, Serbia, Slovakia, Sri Lanka, Syria, Tunisia, Turkmenistan, United Arab Emirates, Uzbekistan, Venezuela, Zimbabwe",
    "JNJ-78436735 (formerly Ad26.COV2.S)": "Andorra, Bahrain, Canada, Austria, Belgium, Bulgaria, Croatia, Cyprus, Czechia, Denmark, Estonia, Finaland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden, Faroe Islands, Greenland, Iceland, Liechtenstein, Norway, Saint Vincent and the Grenadines, United States, WHO",
    "CoronaVac": "Azerbaijan, Bolivia, Brazil, Cambodia, China, Chile, Colombia, Dominican Republic, Ecuador, Hong Kong, Indonesia, Laos, Malaysia, Mexico, Thailand, Tunisia, Turkey, Paraguay, Philippines, Ukraine, Uruguay, Zimbabwe",
    "BBIBP-CorV": "Argentina, Bahrain, Cambodia, China, Egypt, Hungary, Iraq, Jordan, Laos, Macau, Morocco, Nepal, Pakistan, Peru, Senegal, Serbia, Seychelles, United Arab Emirates, Venezuela, Zimbabwe",
    "EpiVacCorona": "Russia, Turkmenistan",
    "Ad5-nCOV": "Mexico, China, Pakistan",
    "Covaxin": "India, Zimbabwe",
    "ZF2001": "China, Uzbekistan",
}

country_names = {
    "UK": "United Kingdom",
    "US": "United States",
    "S. Korea": "South Korea",
    "UAE": "United Arab Emirates",
    "Czechia": "Czech Republic",
    "Côte d'Ivoire": "Cote dIvore",
    "Lao People's Democratic Republic": "Lao PDR",
    "Hong Kong": "Hong Kong SAR China",
    "Macao": "Macao SAR China",
    "Falkland Islands (Malvinas)": "Falkland Islands Malvinas",
}
