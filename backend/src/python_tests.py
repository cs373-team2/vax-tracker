# run unit tests in console : python3 -m unittest python_tests.py

from unittest import main, TestCase
import requests


class Tests(TestCase):

    # ---------
    # Vaccines
    # ---------

    # check expected number of all colleges in response
    def test_vaccinesGET1(self):
        r = requests.get("https://vaxtracker.me/api/vaccines")
        assert r.status_code == 200
        data = r.json()
        assert len(data) == 51

    # check output for specific vaccine
    def test_vaccinesGET2(self):
        r = requests.get("https://vaxtracker.me/api/vaccines?vaccineId=2")
        assert r.status_code == 200
        data = r.json()

        expected =     {
            "vaccine_id": 2,
            "name": "mRNA-1273",
            "mechanism": "mRNA-based vaccine",
            "sponsors": "Moderna",
            "trial_phase": "Phase 3",
            "institutions": "Kaiser Permanente Washington Health Research Institute",
            "efficacy": "94.50%",
            "dosage": "2 doses, 4 weeks apart",
            "storage": "30 days with refrigeration, 6 months at -4\u00b0F (-20\u00b0C)",
            "method": "Muscle injection",
            "logos": "https://static01.nyt.com/newsgraphics/2020/09/10/coronavirus-vaccine-tracker/assets/images/moderna-800.png, https://static01.nyt.com/newsgraphics/2020/09/10/coronavirus-vaccine-tracker/assets/images/nih-800.png",
            "articles": "[{\"source\": {\"id\": \"reuters\", \"name\": \"Reuters\"}, \"author\": \"Reuters Staff\", \"title\": \"Moderna begins study of COVID-19 vaccine in kids - Reuters\", \"description\": \"Moderna Inc has begun dosing patients in a mid-to-late stage study of its COVID-19 vaccine, mRNA-1273, in children aged six months to less than 12 years, the company said on Tuesday.\", \"url\": \"https://www.reuters.com/article/us-health-coronavirus-moderna-idUSKBN2B81EJ\", \"urlToImage\": \"https://static.reuters.com/resources/r/?m=02&d=20210316&t=2&i=1555060688&r=LYNXMPEH2F0T9&w=800\", \"publishedAt\": \"2021-03-16T11:02:00Z\", \"content\": \"By Reuters Staff\\r\\nFILE PHOTO: Nurse Ellen Quinones prepares a dose of the Moderna's coronavirus disease (COVID-19) vaccine at the Bathgate Post Office vaccination facility in the Bronx, in New York, \\u2026 [+852 chars]\"}, {\"source\": {\"id\": \"reuters\", \"name\": \"Reuters\"}, \"author\": \"Reuters Staff\", \"title\": \"BRIEF-Moderna Announces First Participants Dosed In Phase 2/3 Study Of Covid-19 Vaccine Candidate In Pediatric Population - Reuters\", \"description\": \"BRIEF-Moderna Announces First Participants Dosed In Phase 2/3 Study Of Covid-19 Vaccine Candidate In Pediatric Population\\u00a0\\u00a0Reuters\", \"url\": \"https://www.reuters.com/article/brief-moderna-announces-first-participan-idUSB8N2JW0GR\", \"urlToImage\": \"https://s1.reutersmedia.net/resources_v2/images/rcom-default.png?w=800\", \"publishedAt\": \"2021-03-16T11:29:00Z\", \"content\": \"By Reuters Staff\\r\\nMarch 16 (Reuters) -\\r\\n* MODERNA ANNOUNCES FIRST PARTICIPANTS DOSED IN PHASE 2/3 STUDY OF COVID-19 VACCINE CANDIDATE IN PEDIATRIC POPULATION\\r\\n* MODERNA - STUDY WILL EVALUATE SAFETY, \\u2026 [+286 chars]\"}, {\"source\": {\"id\": \"reuters\", \"name\": \"Reuters\"}, \"author\": \"Reuters Staff\", \"title\": \"New study to test Moderna vaccine in transmission prevention among college students - Reuters\", \"description\": \"College students in the United States, vaccinated with Moderna Inc's COVID-19 vaccine, will be part of a new study to test its effectiveness in curbing the spread of the virus, the COVID-19 Prevention Network said on Friday.\", \"url\": \"https://www.reuters.com/article/health-coronavirus-moderna-idUSL4N2LO2BG\", \"urlToImage\": \"https://s1.reutersmedia.net/resources_v2/images/rcom-default.png?w=800\", \"publishedAt\": \"2021-03-26T14:02:54Z\", \"content\": \"By Reuters Staff\\r\\nMarch 26 (Reuters) - College students in the United States, vaccinated with Moderna Incs COVID-19 vaccine, will be part of a new study to test its effectiveness in curbing the sprea\\u2026 [+1329 chars]\"}, {\"source\": {\"id\": \"reuters\", \"name\": \"Reuters\"}, \"author\": \"Reuters Staff\", \"title\": \"Country star Dolly Parton gets her own comic book - Reuters\", \"description\": \"Country singer Dolly Parton is getting her own comic book, the latest addition to TidalWave Comics' \\\"Female Force\\\" series dedicated to inspirational women.\", \"url\": \"https://www.reuters.com/article/uk-people-dolly-parton-comic-book-idUSKBN2BA29M\", \"urlToImage\": \"https://static.reuters.com/resources/r/?m=02&d=20210318&t=2&i=1555373954&r=LYNXMPEH2H19S&w=800\", \"publishedAt\": \"2021-03-18T17:10:00Z\", \"content\": \"By Reuters Staff\\r\\nNEW YORK (Reuters) - Country singer Dolly Parton is getting her own comic book, the latest addition to TidalWave Comics Female Force series dedicated to inspirational women.\\r\\nThe 22\\u2026 [+1192 chars]\"}, {\"source\": {\"id\": \"reuters\", \"name\": \"Reuters\"}, \"author\": \"Reuters Staff\", \"title\": \"New study to test Moderna vaccine in transmission prevention among college students - Reuters\", \"description\": \"College students in the United States, vaccinated with Moderna Inc's COVID-19 vaccine, will be part of a new study to test its effectiveness in curbing the spread of the virus, the COVID-19 Prevention Network said on Friday.\", \"url\": \"https://www.reuters.com/article/us-health-coronavirus-moderna-idUSKBN2BI25R\", \"urlToImage\": \"https://static.reuters.com/resources/r/?m=02&d=20210326&t=2&i=1556342644&r=LYNXMPEH2P16W&w=800\", \"publishedAt\": \"2021-03-26T14:10:56Z\", \"content\": \"FILE PHOTO: A health worker prepares a syringe with a dose of the Moderna COVID-19 vaccine at a vaccination center in Marcq-en-Baroeul as part of the coronavirus disease (COVID-19) vaccination campai\\u2026 [+1467 chars]\"}]",
            "approved_countries": "Andorra, Canada, Austria, Belgium, Bulgaria, Croatia, Cyprus, Czechia, Denmark, Estonia, Finaland, France, Germany, Greece, Hungary, Ireland, Italy, Latvia, Lithuania, Luxembourg, Malta, Netherlands, Poland, Portugal, Romania, Slovakia, Slovenia, Spain, Sweden, Faroe Islands, Greenland, Iceland, Israel, Liechtenstein, Norway, Qatar, Saint Vincent and the Grenadines, Singapore, Switzerland, United Kingdom, United States, Vietnam",
            "num_approved_countries": 42
        }

        data == expected

    def test_vaccinesGET3(self):
        r = requests.get("https://vaxtracker.me/api/vaccines?vaccineId=-12343")
        assert r.status_code == 200
        data = r.json()

        expected = {}

        data == expected

    # ---------
    # Clinical Trials
    # ---------

    # check expected number of instances in clinical trials model
    def test_trialsGET1(self):
        r = requests.get("https://vaxtracker.me/api/trials")
        assert r.status_code == 200
        data = r.json()
        assert len(data) == 530

    # check output for specific trial
    def test_trialsGET2(self):
        r = requests.get("https://vaxtracker.me/api/trials?trialId=3")
        assert r.status_code == 200
        data = r.json()

        expected =   {
            "trial_id": 3,
            "title": "COVID-19 Vaccines International Pregnancy Exposure Registry",
            "status": "Not yet recruiting",
            "study_type": "Observational",
            "study_phase": "N/A",
            "start_date": "2021-04-01 00:00:00",
            "completion_date": "2025-12-31 00:00:00",
            "purpose": "N/A",
            "enrollment": 5000,
            "description": "The objective of the COVID-19 Vaccines International Pregnancy Exposure Registry (C-VIPER) is to evaluate obstetric, neonatal, and infant outcomes among women vaccinated during pregnancy to prevent COVID-19.\n\nSpecifically, the C-VIPER will estimate the risk of obstetric outcomes (abortion, antenatal bleeding, dysfunctional labor, gestational diabetes, hypertensive disorders of pregnancy, intrauterine growth retardation, maternal death, non-reassuring fetal status, pathways to premature birth, postpartum hemorrhage, and COVID-19), neonatal outcomes (congenital anomalies, failure to thrive, low birth weight, neonatal death, neonatal encephalopathy, neonatal infections, preterm birth, respiratory distress in the newborn, small for gestational age, stillbirth, and COVID-19), and infant outcomes (height, weight, health conditions, developmental milestones until one year of age, and COVID-19) among pregnant women exposed to a COVID-19 vaccine from 30 days prior to the first day of the last menstrual period to end of pregnancy and their offspring relative to a matched unexposed reference group.",
            "gender": "Female",
            "minimumAge": "18 Years",
            "maximumAge": "50 Years",
            "country": "United States",
            "vaccines": "COVID-19 vaccine",
            "articles": "[{\"source\": {\"id\": null, \"name\": \"Plos.org\"}, \"author\": \"Hilda Bastian\", \"title\": \"Community Impact Data, 3 New Covid Vaccines, and Trials in Children: A Month of Dilemmas and Good News\", \"description\": \"It\\u2019s been another drama-filled, and data-packed, hectic few weeks in the Covid vaccines landscape. But the visual theme of this roundup post\\u2026\\nThe post Community Impact Data, 3 New Covid Vaccines, and Trials in Children: A Month of Dilemmas and Good News appea\\u2026\", \"url\": \"https://absolutelymaybe.plos.org/2021/03/16/community-impact-data-3-new-covid-vaccines-and-trials-in-children-a-month-of-dilemmas-and-good-news/\", \"urlToImage\": \"https://absolutelymaybe.plos.org/wp-content/uploads/sites/8/2021/03/Cargo-plane-so-excited-scaled.jpg\", \"publishedAt\": \"2021-03-16T23:55:06Z\", \"content\": \"It\\u2019s been another drama-filled, and data-packed, hectic few weeks in the Covid vaccines landscape. But the visual theme of this roundup post is the gigantic global airlift that\\u2019s underway at last, an\\u2026 [+90882 chars]\"}, {\"source\": {\"id\": null, \"name\": \"Orange County Business Journal\"}, \"author\": null, \"title\": \"COVID-19: Vaccines for All Adults Next Month; What Orange Tier Means; Cases Drop - Orange County Business Journal\", \"description\": \"COVID-19: Vaccines for All Adults Next Month; What Orange Tier Means; Cases Drop\\u00a0\\u00a0Orange County Business Journal\", \"url\": \"https://www.ocbj.com/news/2021/mar/25/covid-19-oc-business-updates/\", \"urlToImage\": \"https://ocbj.media.clients.ellingtoncms.com/img/photos/2020/03/19/Newport_Center.jpg\", \"publishedAt\": \"2021-03-26T03:51:21Z\", \"content\": \"The Business Journal is tracking company moves related to the coronavirus. This listing will continue to be updated as the situation changes.\\r\\nSend items to hamanaka@ocbj.com\\r\\nFor more details on man\\u2026 [+301304 chars]\"}, {\"source\": {\"id\": null, \"name\": \"Orange County Business Journal\"}, \"author\": null, \"title\": \"COVID-19: Exploring Yellow Tier as OC Enters Orange Tier; Vaccine Rollout - Orange County Business Journal\", \"description\": \"COVID-19: Exploring Yellow Tier as OC Enters Orange Tier; Vaccine Rollout\\u00a0\\u00a0Orange County Business Journal\", \"url\": \"https://www.ocbj.com/news/2021/apr/01/covid-19-oc-business-updates/\", \"urlToImage\": \"https://ocbj.media.clients.ellingtoncms.com/img/photos/2020/03/19/Newport_Center.jpg\", \"publishedAt\": \"2021-03-31T07:00:00Z\", \"content\": \"The Business Journal is tracking company moves related to the coronavirus. This listing will continue to be updated as the situation changes.\\r\\nSend items to hamanaka@ocbj.com\\r\\nFor more details on man\\u2026 [+305818 chars]\"}]"
        }

        data == expected
    
    def test_trialsGET3(self):
        r = requests.get("https://vaxtracker.me/api/trials?trialId=23484")
        assert r.status_code == 200
        data = r.json()

        expected = {}

        data == expected
        

    # ---------
    # Countries
    # ---------

    # check expected number of instances in Countries model
    def test_countriesGET1(self):
        r = requests.get("https://vaxtracker.me/api/countries")
        assert r.status_code == 200
        data = r.json()
        assert len(data) == 146

    # check output for specific country
    def test_countriesGET2(self):
        r = requests.get("https://vaxtracker.me/api/countries?countryId=2")
        assert r.status_code == 200
        data = r.json()

        expected = {
            "country_id": 2,
            "name": "Albania",
            "iso2": "AL",
            "population": 2866376,
            "capital": "Tirana",
            "flag": "https://upload.wikimedia.org/wikipedia/commons/3/36/Flag_of_Albania.svg",
            "gdp": "13228244357.1813",
            "total_doses_admin": 51690,
            "total_people_vacc": 6073,
            "percent_pop_vacc": "0.21187031987429422",
            "approved_vaccines": 2,
            "total_active_cases": 32931,
            "persons_vaccinated_1plus_dose": 20754.0,
            "total_vaccinations_per100": 0.8,
            "first_vacc_date": "2021-01-17"
        }

        data == expected

    def test_countriesGET3(self):
        r = requests.get("https://vaxtracker.me/api/countries?countryId=-12343")
        assert r.status_code == 200
        data = r.json()

        expected = {}

        data == expected
        
    # ---------
    # Beliefs
    # ---------

    # check expected number of instances in Beliefs model
    def test_beliefsGET1(self):
        r = requests.get("https://vaxtracker.me/api/beliefs")
        assert r.status_code == 200
        data = r.json()
        assert len(data) == 113

    # check output for specific belief
    def test_beliefsGET2(self):
        r = requests.get("https://vaxtracker.me/api/beliefs?beliefId=3")
        assert r.status_code == 200
        data = r.json()

        expected = {
            "id": 3,
            "country_name": "Algeria",
            "flag": "https://upload.wikimedia.org/wikipedia/commons/7/77/Flag_of_Algeria.svg",
            "perc_would_take_vaccine": 0.426619,
            "perc_side_effects": 0.401792,
            "perc_dont_believe": 0.33074,
            "accept_vax_yes": 0.420817,
            "accept_vax_no": 0.296836,
            "accept_vax_idk": 0.282346,
            "mask_extremely": 0.297,
            "mask_very": 0.334,
            "mask_moderately": 0.25,
            "mask_slightly": 0.084,
            "mask_not": 0.035,
            "country_info_id": 3
        }

        data == expected

    def test_beliefsGET3(self):
        r = requests.get("https://vaxtracker.me/api/beliefs?beliefId=-12343")
        assert r.status_code == 200
        data = r.json()

        expected = {}

        data == expected
        
if __name__ == "__main__":  # pragma: no cover
    main()
