from functools import wraps
from typing import Dict, Any, Callable
import json
import yaml
from sqlalchemy.exc import DatabaseError, OperationalError
from flask import Flask, request, Response, abort
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from flask_marshmallow import Marshmallow

config = {}
config.update(yaml.safe_load(open("config.yaml")))
app = Flask(__name__)
CORS(app)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = f"mysql://{config['database']['user']}:{config['database']['password']}@{config['database']['address']}/{config['database']['database']}?charset=utf8mb4"
engine = create_engine(
    f"mysql://{config['database']['user']}:{config['database']['password']}@{config['database']['address']}/{config['database']['database']}?charset=utf8mb4"
)
db = SQLAlchemy(app)
ma = Marshmallow(app)


def jsonify_response(data: Dict[str, Any], code: int = 200) -> Response:
    return Response(
        json.dumps(data, indent=2).encode("utf8"),
        content_type="application/json; charset=utf-8",
        status=code,
    )


def jsonify(func: Callable) -> Callable:
    @wraps(func)
    def decoratedfunction(*args: Any, **kwargs: Any) -> Response:
        return jsonify_response(func(*args, **kwargs))

    return decoratedfunction


def query_db(item_id: str, table: str, id_field: str):
    session = db.session()
    if ";" in item_id:  # SQL injection detected do not proceed
        abort(400)
    if item_id != "-1":
        # Are we grabbing a single row?
        try:
            int(item_id)
            sql = f"SELECT * FROM {table} WHERE {id_field} = {item_id}"
        # Assume we are using a list of item_ids
        except ValueError:
            sql = f"SELECT * FROM {table} WHERE {id_field} IN ({item_id})"
        # Run query in try except block in case item_id isn't valid
        try:
            cursor = session.execute(sql)
        except OperationalError:
            # Query failed due to user error
            abort(400)
        except DatabaseError:
            # Some other db error
            abort(500)
    else:
        cursor = session.execute(f"SELECT * FROM {table}")
    return cursor


@app.route("/", methods=["GET"])
@jsonify
def home():
    return "Not reachable without direct EB url"


@app.route("/api/", methods=["GET"])
@jsonify
def hello_world():
    return {"Name": "Vax Tracker API v0.1"}


@app.route("/api/status", methods=["GET"])
@jsonify
def status():
    return {"code": 200, "message": "All good here"}


@app.route("/api/vaccines", methods=["GET"])
@jsonify
def vaccines():
    # Trivial case where we just grab every row
    if "vaccineId" not in request.args:
        vaccine_id = "-1"  # return list of all vaccines
    else:
        vaccine_id = request.args["vaccineId"]
    cursor = query_db(vaccine_id, "vaccines", "vaccine_id")
    response = []
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
        response.append(row)
    return response


@app.route("/api/vaccines/<int:vaccine_id>", methods=["GET"])  # RESTful endpoint
@jsonify
def single_vaccine(vaccine_id: int):
    cursor = query_db(str(vaccine_id), "vaccines", "vaccine_id")
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
    return row


@app.route("/api/countries", methods=["GET"])
@jsonify
def countries():
    # Trivial case where we just grab every row
    if "countryId" not in request.args:
        country_id = "-1"  # return list of all countries
    else:
        country_id = request.args["countryId"]
    cursor = query_db(country_id, "countries", "country_id")
    response = []
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
        response.append(row)
    return response


@app.route("/api/countries/<int:country_id>", methods=["GET"])  # RESTful endpoint
@jsonify
def single_country(country_id: int):
    cursor = query_db(str(country_id), "countries", "country_id")
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
    return row


@app.route("/api/trials", methods=["GET"])
@jsonify
def clinical_trials():
    # Trivial case where we just grab every row
    if "trialId" not in request.args:
        trial_id = "-1"  # return list of all trials
    else:
        trial_id = request.args["trialId"]
    cursor = query_db(trial_id, "clinical_trials", "trial_id")
    response = []
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
        row["completion_date"] = str(row["completion_date"])
        row["start_date"] = str(row["start_date"])
        response.append(row)
    return response


@app.route("/api/trials/<int:trial_id>", methods=["GET"])  # RESTful endpoint
@jsonify
def single_trial(trial_id: int):
    cursor = query_db(str(trial_id), "clinical_trials", "trial_id")
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
        row["completion_date"] = str(row["completion_date"])
        row["start_date"] = str(row["start_date"])
    return row


@app.route("/api/beliefs", methods=["GET"])
@jsonify
def beliefs():
    # Trivial case where we just grab every row
    if "beliefId" not in request.args:
        belief_id = "-1"  # return list of all trials
    else:
        belief_id = request.args["beliefId"]
    cursor = query_db(belief_id, "beliefs", "id")
    response = []
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
        response.append(row)
    return response


@app.route("/api/beliefs/<int:belief_id>", methods=["GET"])  # RESTful endpoint
@jsonify
def single_belief(belief_id: int):
    cursor = query_db(str(belief_id), "beliefs", "id")
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
    return row


@app.route("/api/vaccine_link", methods=["GET"])
@jsonify
def vaccine_link():
    num_args = 0
    table = "vaccine_country_rel"
    if "vaccineId" not in request.args:
        vaccine_id = "-1"
    else:
        num_args += 1
        vaccine_id = request.args["vaccineId"]
    if "countryId" not in request.args:
        country_id = "-1"
    else:
        num_args += 1
        country_id = request.args["countryId"]
    if num_args == 2:
        abort(400)
    if (
        vaccine_id == country_id or vaccine_id != "-1"
    ):  # Query everything or query vaccine
        cursor = query_db(vaccine_id, table, "vaccine_id")
    elif country_id != "-1":
        cursor = query_db(country_id, table, "country_id")
    response = []
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
        response.append(row)
    return response


@app.route("/api/trial_link", methods=["GET"])
@jsonify
def trial_link():
    num_args = 0
    table = "trial_country_rel"
    if "trialId" not in request.args:
        trial_id = "-1"
    else:
        num_args += 1
        trial_id = request.args["trialId"]
    if "countryId" not in request.args:
        country_id = "-1"
    else:
        num_args += 1
        country_id = request.args["countryId"]
    if num_args == 2:
        abort(400)
    if trial_id == country_id or trial_id != "-1":  # Query everything or query vaccine
        cursor = query_db(trial_id, table, "trial_id")
    elif country_id != "-1":
        cursor = query_db(country_id, table, "country_id")
    response = []
    row = {}
    for result in cursor.fetchall():
        row = dict(result)
        response.append(row)
    return response


@app.route("/api/belief_link", methods=["GET"])
@jsonify
def belief_link():
    if "countryId" not in request.args:
        country_id = "-1"
    else:
        country_id = request.args["countryId"]
    cursor = query_db(country_id, "beliefs", "country_info_id")
    response = []
    row = {}
    for result in cursor.fetchall():
        row["belief_id"] = result["id"]
        row["country_id"] = result["country_info_id"]
        response.append(row)
        row = {}
    return response


if __name__ == "__main__":
    app.run(port=8080, debug=True)


# CORS(app)
# # NOTE: This route is needed for the default EB health check route
# @app.route('/')
# def home():
#     return "ok"
# @app.route('/api/get_topics')
# def get_topics():
#     return {"topics": ["topic1", "other stuff", "next topic"]}
# @app.route('/api/submit_question', methods=["POST"])
# def submit_question():
#     question = json.loads(request.data)["question"]
#     return {"answer": f"You Q was {len(question)} chars long"}
# if __name__ == '__main__':
#     app.run(port=8080)
