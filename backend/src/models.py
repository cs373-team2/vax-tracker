from app import db, ma
from datetime import datetime


vaccine_country_rel = db.Table(
    "vaccine_country_rel",
    db.Column("vaccine_id", db.Integer, db.ForeignKey("vaccines.vaccine_id")),
    db.Column("country_id", db.Integer, db.ForeignKey("countries.country_id")),
)
trial_country_rel = db.Table(
    "trial_country_rel",
    db.Column("trial_id", db.Integer, db.ForeignKey("clinical_trials.trial_id")),
    db.Column("country_id", db.Integer, db.ForeignKey("countries.country_id")),
)

class VaccineNews(db.Model):
    __tablename__ = "vaccine_news"
    id = db.Column(db.Integer(), primary_key=True)
    articles = db.Column(db.String(10000))

class VaccineNewsSchema(ma.Schema):
    class Meta:
        model = VaccineNews
        load_instance = True

class Vaccine(db.Model):
    __tablename__ = "vaccines"
    vaccine_id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255))
    mechanism = db.Column(db.String(255), nullable=False, default='N/A')
    sponsors = db.Column(db.String(255), nullable=False, default='N/A')
    trial_phase = db.Column(db.String(255), nullable=False, default='N/A')
    institutions = db.Column(db.String(255), nullable=False, default='N/A')
    efficacy = db.Column(db.String(255), nullable=False, default='Unknown')
    dosage = db.Column(db.String(255), nullable=False, default='TBD')
    storage = db.Column(db.String(255), nullable=False, default='N/A')
    method = db.Column(db.String(255), nullable=False, default='N/A')
    logos = db.Column(db.String(600), nullable=False, default='')
    articles = db.Column(db.String(10000))
    approved_countries = db.Column(db.String(1000), nullable=False, default='N/A')
    num_approved_countries = db.Column(db.Integer, nullable=False, default=0)
    approved_countries_list = db.relationship(
        "Country", secondary=vaccine_country_rel, backref="approved_vaccines_list"
    )


class VaccineSchema(ma.Schema):
    class Meta:
        model = Vaccine
        load_instance = True


class ClinicalTrial(db.Model):
    __tablename__ = "clinical_trials"
    trial_id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(255))
    status = db.Column(db.String(255))
    study_type = db.Column(db.String(255))
    study_phase = db.Column(db.String(255))
    start_date = db.Column(db.DateTime())
    completion_date = db.Column(db.DateTime())
    purpose = db.Column(db.String(255))
    enrollment = db.Column(db.Integer(), nullable=False, default=0)
    description = db.Column(db.String(2000))
    gender = db.Column(db.String(255))
    minimumAge = db.Column(db.String(255))
    maximumAge = db.Column(db.String(255))
    country = db.Column(db.String(255))
    vaccines = db.Column(db.String(800))
    articles = db.Column(db.String(10000))
    participating_countries = db.relationship(
        "Country", secondary=trial_country_rel, backref="clinical_trials"
    )


class ClinicalTrialSchema(ma.Schema):
    class Meta:
        model = ClinicalTrial
        load_instance = True


class Country(db.Model):
    __tablename__ = "countries"
    country_id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255))
    iso2 = db.Column(db.String(255))
    population = db.Column(db.Integer(), nullable=False, default=0)
    capital = db.Column(db.String(255), nullable=False, default=0)
    flag = db.Column(db.String(255), nullable=False, default=0)
    gdp = db.Column(db.String(255), nullable=False, default=0)
    total_doses_admin = db.Column(db.Integer(), nullable=False, default=0)
    total_people_vacc = db.Column(db.Integer(), nullable=False, default=0)
    percent_pop_vacc = db.Column(db.String(255), nullable=False, default=0)
    approved_vaccines = db.Column(db.Integer(), nullable=False, default=0)
    total_active_cases = db.Column(db.Integer(), nullable=False, default=0)
    persons_vaccinated_1plus_dose = db.Column(db.Float(), nullable=False, default=0)
    total_vaccinations_per100 = db.Column(db.Float(), nullable=False, default=0)
    first_vacc_date = db.Column(db.String(255))

class CountrySchema(ma.Schema):
    class Meta:
        model = Country
        load_instance = True


class Belief(db.Model):
    __tablename__ = "beliefs"
    id = db.Column(db.Integer(), primary_key=True)
    country_name = db.Column(db.String(255))
    flag = db.Column(db.String(255), nullable=False, default=0)
    perc_would_take_vaccine = db.Column(db.Float(), nullable=False, default=0)
    perc_side_effects = db.Column(db.Float(), nullable=False, default=0)
    perc_dont_believe = db.Column(db.Float(), nullable=False, default=0)
    accept_vax_yes = db.Column(db.Float(), nullable=False, default=0)
    accept_vax_no = db.Column(db.Float(), nullable=False, default=0)
    accept_vax_idk = db.Column(db.Float(), nullable=False, default=0)
    mask_extremely = db.Column(db.Float(), nullable=False, default=0)
    mask_very = db.Column(db.Float(), nullable=False, default=0)
    mask_moderately = db.Column(db.Float(), nullable=False, default=0)
    mask_slightly = db.Column(db.Float(), nullable=False, default=0)
    mask_not = db.Column(db.Float(), nullable=False, default=0)
    country_info_id = db.Column(db.Integer(), db.ForeignKey("countries.country_id"))
    country_info = db.relationship(
        "Country", backref=db.backref("beliefs", uselist=False)
    )


class BeliefSchema(ma.Schema):
    class Meta:
        model = Belief
        load_instance = True
