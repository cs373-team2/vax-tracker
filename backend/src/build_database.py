import os
from app import db, engine
from datetime import datetime
from models import Vaccine, ClinicalTrial, Country, Belief, VaccineNews, vaccine_country_rel
from approved_countries import approved_countries, country_names
from country_data import *
from newsapi import NewsApiClient
import requests
import json
import pandas as pd


def format_time(s):
    while True:
        try:
            d = datetime.strptime(s, "%B %d, %Y")
            break
        except ValueError:
            d = datetime.strptime(s, "%B %Y")
            break
    return d.date()


def get_data(entry, field):
    try:
        return entry[field][0]
    except IndexError:
        return "N/A"


def arr_to_str(entry, str):
    s = set(entry[str])
    if not s:
        return "N/A"
    return ", ".join(s)


def ct_instance(trial):
    brief_title = get_data(trial, "BriefTitle")
    overall_status = get_data(trial, "OverallStatus")
    study = get_data(trial, "StudyType")
    phase = get_data(trial, "Phase")
    purpose = get_data(trial, "DesignPrimaryPurpose")
    enrollment = get_data(trial, "EnrollmentCount")
    start = format_time(trial["StartDate"][0])
    completion = format_time(trial["CompletionDate"][0])
    country = arr_to_str(trial, "LocationCountry")
    description = get_data(trial, "BriefSummary")
    gender = get_data(trial, "Gender")
    minimum_age = get_data(trial, "MinimumAge")
    maximum_age = get_data(trial, "MaximumAge")
    vaccines = arr_to_str(trial, "InterventionName")
    return ClinicalTrial(
        title=brief_title,
        status=overall_status,
        study_type=study,
        study_phase=phase,
        start_date=start,
        completion_date=completion,
        purpose=purpose,
        enrollment=enrollment,
        country=country,
        description=description,
        gender=gender,
        minimumAge=minimum_age,
        maximumAge=maximum_age,
        vaccines=vaccines
    )


def create_trials_table():
    trials = requests.get(
        "https://ClinicalTrials.gov/api/query/study_fields?expr=covid19+vaccine&fields=BriefTitle,StudyType,OverallStatus,Phase,StartDate,CompletionDate,DesignPrimaryPurpose,EnrollmentCount,LocationCountry,Gender,MinimumAge,MaximumAge,BriefSummary,InterventionName&min_rnk=1&max_rnk=1000&fmt=json"
    ).text
    trials_data = json.loads(trials)
    for trial in trials_data["StudyFieldsResponse"]["StudyFields"]:
        try:
            t = ct_instance(trial)
            db.session.add(t)
        except IndexError:
            continue
    db.session.commit()


def get_approved_countries(vaccine):
    if vaccine in approved_countries:
        return approved_countries[vaccine]
    else:
        return "N/A"


def create_vaccines_table():
    vaccines = requests.get("https://disease.sh/v3/covid-19/vaccine").text
    vacc_data = json.loads(vaccines)
    for vax in vacc_data["data"]:
        approved_countries = get_approved_countries(vax["candidate"])
        v = Vaccine(
            name=vax["candidate"],
            mechanism=vax["mechanism"],
            sponsors=arr_to_str(vax, "sponsors"),
            trial_phase=vax["trialPhase"],
            institutions=arr_to_str(vax, "institutions"),
            approved_countries=approved_countries,
            num_approved_countries=get_num_approved_countries(approved_countries)
        )
        db.session.add(v)

    db.session.commit()

def get_num_approved_countries(countries):
    if countries != 'N/A':
        arr = countries.split(', ')
        return len(arr)
    else:
        return 0

def get_vaccine_csv():
    df = pd.read_csv("vaccines_data.csv")
    df.dropna(inplace=True)
    vaccines = Vaccine.query.all()
    for vaccine in vaccines:
        if vaccine.name in df.values:
            vaccine.efficacy = df.loc[df["name"] == vaccine.name, "efficacy"].iloc[0]
            vaccine.dosage = df.loc[df["name"] == vaccine.name, "dosage"].iloc[0]
            vaccine.storage = df.loc[df["name"] == vaccine.name, "storage"].iloc[0]
            vaccine.method = df.loc[df["name"] == vaccine.name, "method"].iloc[0]
            vaccine.logos = df.loc[df["name"] == vaccine.name, "logo"].iloc[0]
    
    db.session.commit()


def get_population(country):
    return make_api_call("population", country)


def make_api_call(attribute, country):
    if country in country_names:
        country = country_names[country]

    if attribute == "population":
        url = "https://countriesnow.space/api/v0.1/countries/population"
    elif attribute == "capital":
        url = "https://countriesnow.space/api/v0.1/countries/capital"
    elif attribute == "flag":
        url = "https://countriesnow.space/api/v0.1/countries/flag/images"

    payload = '{\n\t"country": "%s"\n}' % (country)
    headers = {"Content-Type": "application/json"}
    response = requests.request("POST", url, headers=headers, data=payload).text
    try:
        data = json.loads(response)["data"]
        if attribute == "population":
            data = data["populationCounts"]
            length = len(data)
            if length:
                return int(data[length - 1]["value"])
            else:
                return "N/A"
        elif attribute == "capital":
            data = data["capital"]
            return data
        elif attribute == "flag":
            data = data["flag"]
            return data
    except KeyError:
        print(country, "making api call")
        return "N/A"


def get_capital(country):
    return make_api_call("capital", country)


def get_flag(country):
    return make_api_call("flag", country)


def get_active_cases(country):
    if country in country_names:
        country = country_names[country]
    format_country = country.replace(" ", "-").lower()
    url = (
        "https://api.covid19api.com/live/country/%s/status/confirmed/date/2020-03-24"
        % format_country
    )
    payload = {}
    headers = {"Content-Type": "application/json"}
    try:
        response = requests.request("GET", url, headers=headers, data=payload).text
        try:
            data = json.loads(response)
            length = len(data)
            if length:
                return data[length - 1]["Active"]
            else:
                return "N/A"
        except ValueError:
            print(country, "jsonDecodeError")
    except KeyError:
        print("key error")
        print(country)


def get_people_vaccinated(country):
    try:
        return country_pop_data.loc[
            country_pop_data["location"] == country, "people_vaccinated"
        ].iloc[0]
    except IndexError:
        print(country, "getting people vacc")

    # print(country_pop_data.query('location == {country}')['people_vaccinated'])
    # return country_pop_data.query('location == {country}')['people_vaccinated'][0]
    # country_pop_data.loc[country_pop_data['location'] == country,]
    # db.session.commit()


def get_iso2():
    url = "https://api.covid19api.com/countries"
    payload = {}
    headers = {"Content-Type": "application/json"}
    response = requests.request("GET", url, headers=headers, data=payload).text
    data = json.loads(response)
    for c in data:
        country = Country.query.filter_by(name=c["Country"]).one_or_none()
        if not country:
            continue

        iso_2 = c.get("ISO2")
        if not iso_2:
            continue

        country.iso2 = iso_2

    db.session.commit()

def get_country_vaccine_data():
    df = pd.read_csv('country_vaccination_data.csv')
    df.fillna(0, inplace=True)
    countries = Country.query.all()
    for country in countries:
        try:
            country.persons_vaccinated_1plus_dose = df.loc[df["COUNTRY"] == country.name, "PERSONS_VACCINATED_1PLUS_DOSE"].iloc[0]
            country.total_vaccinations_per100 = df.loc[df["COUNTRY"] == country.name, "TOTAL_VACCINATIONS_PER100"].iloc[0]
            country.first_vacc_date = df.loc[df["COUNTRY"] == country.name, "FIRST_VACCINE_DATE"].iloc[0]
            country.approved_vaccines =  int(df.loc[df["COUNTRY"] == country.name, "NUMBER_VACCINES_TYPES_USED"].iloc[0])
        except IndexError:
            print(country.name)
    db.session.commit()

def get_country_gdp(country):
    try:
        return country_gdp_data.loc[
            country_gdp_data["Country Name"] == country, "Value"
        ].iloc[0]
    except IndexError:
        print(country, "getting gdp")


def create_countries_table():

    global_doses = requests.get("https://disease.sh/v3/covid-19/vaccine/coverage/countries?lastdays=1").text
    doses_data = json.loads(global_doses)

    for country in doses_data:
      c = Country(name=country['country'], total_doses_admin=country['timeline'][datetime.now().date().strftime("%-m/%-d/%y")])
      db.session.add(c)

    db.session.commit()

    countries = Country.query.all()
    for country in countries:
        country.population = get_population(country.name)
        country.capital = get_capital(country.name)
        country.flag = get_flag(country.name)
        country.total_active_cases = get_active_cases(country.name)
        get_iso2()
        country.total_people_vacc = get_people_vaccinated(country.name)
        # country.gdp = get_country_gdp(country.name)

        # try:
        #     if (country.total_people_vacc != 'nan' country.total_people_vacc != '' or country.total_people_vacc != ' '):
        #         country.percent_pop_vacc = (int(float(country.total_people_vacc))) / country.population * 100
        #     country.percent_pop_vacc = float(country.total_people_vacc) / country.population * 100
        # except ValueError:
        #     pass

        db.session.commit()


def create_beliefs_table():
    df = pd.read_csv("countries_to_iso2.csv")
    header = "smoothed_"
    indicators = ["barrier_reason_wontwork", "hesitant_sideeffects"]
    beliefs1 = requests.get(
        "https://covidmap.umd.edu/api/resources?indicator=vaccine_acpt&type=smoothed&country=all&date=20210309"
    ).text
    beliefs_data1 = json.loads(beliefs1)
    for country in beliefs_data1["data"]:
        country_query = Country.query.filter(
            Country.name == country["country"]
        ).one_or_none()
        flag = ""
        if country_query is None:
            print(country["country"])
        else:
            flag = country_query.flag

        vaccine_accept = country[header + "vu"]
        wont_work = 0
        side_effects = 0
        for indicator in indicators:
            ind = requests.get(
                "https://covidmap.umd.edu/api/resources?indicator={0}&type=smoothed&country={1}&date=20210309".format(
                    indicator, country["country"]
                )
            ).text
            ind_data = json.loads(ind)
            if indicator == "barrier_reason_wontwork":
                if ind_data["data"]:
                    wont_work = ind_data["data"][0][header + "pct_" + indicator]
            else:
                if ind_data["data"]:
                    side_effects = ind_data["data"][0][header + indicator]
        b = Belief(
            country_name=country["country"],
            perc_would_take_vaccine=vaccine_accept,
            perc_dont_believe=wont_work,
            perc_side_effects=side_effects,
            flag=flag,
        )
        db.session.add(b)

    db.session.commit()

    beliefs_db = Belief.query.all()
    for belief in beliefs_db:
        beliefs = requests.get(
            "http://covidsurvey.mit.edu:5000/query?country=%s&signal={vaccine_accept,effect_mask}"
            % df.loc[df["name"] == belief.country_name, "iso2"].iloc[0]
        ).text
        beliefs_data = json.loads(beliefs)
        try:
            belief.accept_vax_yes = float(
                beliefs_data["vaccine_accept"]["weighted"]["Yes"]
            )
            belief.accept_vax_no = float(
                beliefs_data["vaccine_accept"]["weighted"]["No"]
            )
            belief.accept_vax_idk = float(
                beliefs_data["vaccine_accept"]["weighted"]["Don't know"]
            )
            belief.mask_extremely = float(
                beliefs_data["effect_mask"]["weighted"]["Extremely effective"]
            )
            belief.mask_very = float(
                beliefs_data["effect_mask"]["weighted"]["Very effective"]
            )
            belief.mask_moderately = float(
                beliefs_data["effect_mask"]["weighted"]["Moderately effective"]
            )
            belief.mask_slightly = float(
                beliefs_data["effect_mask"]["weighted"]["Slightly effective"]
            )
            belief.mask_not = float(
                beliefs_data["effect_mask"]["weighted"]["Not effective at all"]
            )
        except KeyError:
            continue

    db.session.commit()


def create_country_vaccine_rel():
    countries = Country.query.all()
    country_name_to_orm = {c.name: c for c in countries}
    vaccines = Vaccine.query.all()
    for vaccine in vaccines:
        country_list = vaccine.approved_countries.split(", ")
        country_orm_list = [
            country_name_to_orm[c] for c in country_list if country_name_to_orm.get(c)
        ]
        vaccine.approved_countries_list = country_orm_list
    db.session.commit()


def create_country_trial_rel():
    countries = Country.query.all()
    country_name_to_orm = {c.name: c for c in countries}
    trials = ClinicalTrial.query.all()
    for trial in trials:
        country_list = trial.country.split(", ")
        country_orm_list = [
            country_name_to_orm[c] for c in country_list if country_name_to_orm.get(c)
        ]
        trial.participating_countries = country_orm_list
    db.session.commit()


def create_country_belief_rel():
    beliefs = Belief.query.all()
    for belief in beliefs:
        try:
            belief.country_info_id = (
                Country.query.filter(Country.name == belief.country_name)
                .first()
                .country_id
            )
        except AttributeError:
            print(belief.country_name)
            continue
    db.session.commit()

def get_vaccine_news():
    newsapi = NewsApiClient(api_key='c441b202b9f346f18b3b44fd6c7f8705')
    vaccines = Vaccine.query.all()
    for vaccine in vaccines:
        keyword = ''
        print(vaccine.name)
        if (vaccine.name != 'No name announced'):
            keyword = vaccine.name
        else:
            keyword = vaccine.sponsors
        
        if not keyword:
            articles = 'None'
        else:
            top_headlines = newsapi.get_everything(q=keyword, page_size=5, sort_by='popularity')
            json_obj = json.dumps(top_headlines['articles'])
            # articles = str(top_headlines['articles']).encode("utf-8")
            # print(articles)
        vaccine.articles = str(json_obj)
    
    db.session.commit()

def get_trials_news():
    newsapi = NewsApiClient(api_key='55d6c7226def426b8dfc1fb14f660b5e')
    trials = ClinicalTrial.query.all()
    for trial in trials:
        keyword = trial.title
        top_headlines = newsapi.get_everything(q=keyword, page_size=5, sort_by='popularity')
        json_obj = json.dumps(top_headlines['articles'])
        # print(trial.title)
        # print(str(json_obj))
        trial.articles = str(json_obj)
        db.session.commit()

def find_trial_vaccine():
    trials = ClinicalTrial.query.all()
    vaccineKeywords = dict()
    vaccines = Vaccine.query.all()
    for vaccine in vaccines:
        keywords = list()
        keywords.append(vaccine.name)
        keywords.append(vaccine.sponsors)
        keywords.append(vaccine.mechanism)
        keywords.append(vaccine.institutions)
        vaccineKeywords[vaccine.name] = keywords
    total = 0
    nonEmpty = 0
    for trial in trials:
        vaccineIds = list()
        for vaccineId in vaccineKeywords:
            if any(trial.vaccines in string for string in vaccineKeywords[vaccineId]):
                vaccineIds.append(vaccineId)
        total += 1
        if vaccineIds:
            nonEmpty += 1
    print(total, nonEmpty)

def dump_country_data():
    country_data = Country.query.all()
    for c in country_data:
        print(c)

# def getGlobalVizData():
#     countries = Country.query.all()
#     result = dict()
#     for country in countries:
        
def getBeliefsVizData():
    beliefs = Belief.query.all()
    result = list()
    for belief in beliefs:
        beliefDict = dict()
        beliefDict['country'] = belief.country_name
        beliefDict['perc_take_vaccine'] = belief.perc_would_take_vaccine
        result.append(beliefDict)
    print(result)

def getVaccinePhaseData():
    vaccines = Vaccine.query.all()
    result = list()
    tempDict = dict()
    for vaccine in vaccines:
        trial_phase = vaccine.trial_phase
        if trial_phase not in tempDict:
            tempDict[trial_phase] = 1
        else:
            temp = tempDict[trial_phase]
            tempDict[trial_phase] = temp + 1
    for key in tempDict:
        newDict = dict()
        newDict['trial_phase'] = key
        newDict['num_vaccines'] = tempDict[key]
        result.append(newDict)
    print(result)

def getGlobalVaccData():
    countries = Country.query.all()
    result = list()
    for country in countries:
        temp = list()
        temp.append(country.iso2.lower())
        temp.append(country.total_vaccinations_per100)
        result.append(temp)
    print(result)

def convertHotelRatings():
    df = pd.read_csv("./data/countries_to_iso2.csv")
    with open('hotel_ratings.json') as json_file:
        data = json.load(json_file)
    result = list()
    for elem in data:
        newList = list()
        iso2 = df.loc[df["name"] == elem["country"], "iso2"].iloc[0]
        newList.append(iso2)
        newList.append(elem["avg_hotel_rating"])
        result.append(newList)
    print(result)

def main():
#     country = Country.query.filter(Country.name == 'Belgium').one_or_none()
#     print(country.beliefs.id)

    # ClinicalTrial.__table__.drop(engine)
    # ClinicalTrial.__table__.create(engine)

    # db.drop_all()
    # db.create_all()

    # create_countries_table()
    # create_beliefs_table()
    # create_trials_table()
    # create_vaccines_table()
    # create_country_belief_rel()
    # create_country_trial_rel()
    # create_country_vaccine_rel()
    # get_country_vaccine_data()
    # get_vaccine_csv()
    # find_trial_vaccine()
    # get_vaccine_news()
    # get_trials_news()
    # getBeliefsVizData()
    # getVaccinePhaseData()
    # getGlobalVaccData()
    convertHotelRatings()

main()
