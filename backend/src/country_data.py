import pandas as pd
import requests
import io

# Downloading the csv file from your GitHub account

url = "https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/vaccinations.csv?raw=true"
download = requests.get(url).content

df = pd.read_csv(io.StringIO(download.decode("utf-8")))

# country_pop_data = df.query(
# 'date == "2021-02-24"')[["location", "people_vaccinated"]]

country_pop_data = (
    df.sort_values("people_vaccinated", ascending=False)
    .drop_duplicates("location")
    .sort_index()
)


url2 = "https://raw.githubusercontent.com/datasets/gdp/master/data/gdp.csv"
download2 = requests.get(url2).content

df2 = pd.read_csv(io.StringIO(download2.decode("utf-8")))

country_gdp_data = (
    df2.sort_values("Value", ascending=False)
    .drop_duplicates("Country Name")
    .sort_index()
)
# print(country_pop_data[['location', 'people_vaccinated']])
# print(country_pop_data[country_pop_data['people_vaccinated'].isnull()])
country_gdp_data["people_vaccinated"] = pd.to_numeric(
    df["people_vaccinated"], errors="coerce"
)
country_gdp_data = country_gdp_data.dropna(subset=["people_vaccinated"])
country_gdp_data["people_vaccinated"] = country_gdp_data["people_vaccinated"].astype(
    int
)
# print(country_pop_data[['location', 'people_vaccinated']])
